#ifndef ZRMCELLVIEW_H
#define ZRMCELLVIEW_H

#include "ui_zrmcellview.h"
#include <zrmbasewidget.h>

class CellsStats;

class ZrmCellView : public ZrmChannelWidget, private Ui::ZrmCellViewMinimal
{
    Q_OBJECT
public:
    explicit ZrmCellView(QWidget* parent = nullptr);
    enum VerticalColumnRoles : int {vNumber, vVolt, vTemp};
    enum HorizontalItemRoles : int {hVolt, hTemp};
    int getRowsCount() const;
    QSize sizeHint() const override;
    Qt::Orientation getOrientation() {return m_orientation;}
    void setOrientation(Qt::Orientation orientation);
    void showDelta(bool show);

protected:
    virtual void resizeEvent(QResizeEvent* re) override;
    virtual void showEvent(QShowEvent*    se) override;
    virtual void channel_param_changed(unsigned channel, const zrm::params_list_t& params_list  ) override;
    virtual void update_controls() override;
    virtual void clear_controls() override;
    virtual void channel_session(unsigned ch_num) override;
    virtual void onActivate() override ;
    virtual void onDeactivate() override ;

private slots:
    void subscribeCells(bool force = false);
    void deltaChanged (double);

private:
    void setCellValues(int number, const zrm::zrm_cell_t& cell, const CellsStats& cs);
    void makeHorizontalCells();
    void makeVerticalCells();

    void startQueryTimer();
    void initCellsTree();
    void cellsCount(uint16_t ccnt);
    void cellsParam() ;
    void updateColumnWidth() ;
    Qt::Orientation m_orientation = Qt::Horizontal;
    static constexpr const char* numberText = "№";
    static constexpr const char* voltText = "U";
    static constexpr const char* tempText = "T";
    QColor normalBackground, alterBackground, normalText;
    QColor outBoundBackground, outBoundText;
    QColor colorMaxU = QColor(0, 200, 25), colorMinU = QColor(250, 0, 180), colorMaxT = QColor(210, 180, 0);
    int32_t max_volt = 0, min_volt = INT32_MAX;
    int32_t max_temp = 0;
};

#endif // ZRMCELLVIEW_H
