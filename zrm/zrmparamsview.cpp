﻿#include "zrmparamsview.h"
#include <zrmparamcvt.h>
#include <QMessageBox>
#include <QInputDialog>
#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QDateTime>

namespace {

class ItemDelegate: public QStyledItemDelegate
{
public:
	ItemDelegate() = default;
	QWidget* createEditor(QWidget* parent,
						  const QStyleOptionViewItem& option,
						  const QModelIndex& index) const override;
	void setEditorData(QWidget* editor, const QModelIndex& index) const override;
	void setModelData(QWidget* editor,
					  QAbstractItemModel* model,
					  const QModelIndex& index) const override;


};

QWidget* ItemDelegate::createEditor(QWidget* parent,
									const QStyleOptionViewItem& option,
									const QModelIndex& index) const
{
	Q_UNUSED(option)

	if (index.column() != ZrmParamsView::column_new_value)
		return nullptr;

	QSpinBox* sb = new QSpinBox(parent);
	return sb;
}

void ItemDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
	QSpinBox* sb = qobject_cast<QSpinBox*>(editor);
	if (sb)
	{
		int value = index.model()->index(index.row(), ZrmParamsView::column_value).data().toInt();
		sb->setValue(value);
		int valueMax = index.model()->index(index.row(), ZrmParamsView::column_new_value).data(Qt::UserRole).toInt();
		if (valueMax > 0)
			sb->setMaximum(valueMax);
	}
	else
	{
		QStyledItemDelegate::setEditorData(editor, index);
	}
}

void ItemDelegate::setModelData(QWidget* editor,
								QAbstractItemModel* model,
								const QModelIndex& index) const
{
	QStyledItemDelegate::setModelData(editor, model, index);
}
} // end namspace


ZrmParamsView::ZrmParamsView(QWidget* parent) :
	ZrmChannelWidget (parent)
{
	setupUi(this);
	editableIcon = QIcon(":/zrm/icons/edit_2.png");

	QHeaderView* hdr = zrm_params->header();
	hdr->setSectionResizeMode(QHeaderView::ResizeMode::ResizeToContents);
	tbWriteParams->setDefaultAction(actWriteParameters);
	tbUndoParameters->setDefaultAction(actUndoParameters);
	tbServiceMode->setDefaultAction(actServiceMode);
	tbSynchronizationClock->setDefaultAction(actSynchronizationClock);
	zrm_params->setItemDelegate(new ItemDelegate);
	init_params();
	initSlotConnection();
}

void ZrmParamsView::initSlotConnection()
{
	connect(&requestTimer, &QTimer::timeout, this, &ZrmParamsView::request);
	connect(actServiceMode, &QAction::triggered, this, &ZrmParamsView::serviceMode);
	connect(actSynchronizationClock, &QAction::triggered, this, &ZrmParamsView::synchronizationClock);
	connect(actWriteParameters, &QAction::triggered, this, &ZrmParamsView::writeParameters);
	connect(actUndoParameters, &QAction::triggered, this, &ZrmParamsView::undoParameters);
	connect(passwd, &QLineEdit::textChanged, this, &ZrmParamsView::passwdChanged);
	connect(zrm_params, &QTreeWidget::itemChanged, this, &ZrmParamsView::paramChanged);
}

void ZrmParamsView::appendParam(zrm::zrm_param_t param, const QString& text, bool ordered, bool editable, int max)
{
	if (ordered)
		orders.push_back( param );
	else
		queryParms.push_back(param);

	QTreeWidget* parent = zrm_params;

	QTreeWidgetItem* item =   new QTreeWidgetItem(parent, QStringList() << text);
	if (editable)
	{
		item->setFlags(item->flags() | Qt::ItemIsEditable);
		item->setIcon(column_new_value, editableIcon);
		if (max > 0)
			item->setData(ZrmParamsView::column_new_value, Qt::UserRole, max);
		editableItems.append(item);
	}

	items.insert(param, item );
}

void ZrmParamsView::init_params()
{
	appendParam(zrm::PARAM_VRDEV, tr("Версия блока"), false);
	appendParam(zrm::PARAM_RVDEV, tr("Модификация блока"), false);
	appendParam(zrm::PARAM_RVSW, tr("Версия ПО"), false);
	appendParam(zrm::PARAM_SOFT_REV, tr("Модификация ПО"), false);
	appendParam(zrm::PARAM_SERNM, tr("Заводской номер"), false);
	appendParam(zrm::PARAM_MEMFR, tr("Свободная память"), true);

	appendParam(zrm::PARAM_CUR, tr("Ток"), true);
	appendParam(zrm::PARAM_LCUR, tr("Ограничение тока"), true);
	appendParam(zrm::PARAM_VOLT, tr("Напряжение"), true);
	appendParam(zrm::PARAM_LVOLT, tr("Оганичение напряжения"), true);
	appendParam(zrm::PARAM_CAP, tr("Ёмкость"), true);
	appendParam(zrm::PARAM_STG_NUM, tr("Номер этапа"), true);
	appendParam(zrm::PARAM_LOOP_NUM, tr("Номер цикла"), true);
	appendParam(zrm::PARAM_TRECT, tr("Температура"), true);
	appendParam(zrm::PARAM_ST, tr("Выполняемая операция"), true);
	appendParam(zrm::PARAM_VOUT, tr("Напряжение на выходе ЗРМ"), true);
	appendParam(zrm::PARAM_POWER, tr("Мощность"), true);
	appendParam(zrm::PARAM_SYNCHRONIZATION_CLOCK, tr("Время устройства"), true);
	appendParam(zrm::PARAM_MVOLT, tr("Макс. напряжение"), false);

	appendParam(zrm::PARAM_DPOW, tr("Макс. мощность разряда"), false);
	appendParam(zrm::PARAM_MAX_CHP, tr("Макс. мощность заряда"), false);
	appendParam(zrm::PARAM_MCUR, tr("Макс. ток"), true);
	appendParam(zrm::PARAM_MCURD, tr("Макс. ток разряда"), true);

	appendParam(zrm::PARAM_CUR_CONSUMPTION, tr("Потребляемый ток"), true);
	appendParam(zrm::PARAM_VOLT_SUPPLY, tr("Напряжение питающей сети"), true);
	appendParam(zrm::PARAM_VOLT_HIGH_VOLT_BUS, tr("Напряжение высоковольтной шины"), true);
	appendParam(zrm::PARAM_FAN_PERCENT, tr("Вентиляторы"), true);

	respond =   new QTreeWidgetItem(zrm_params, QStringList() << tr("Время ответа канала"));

	appendParam(zrm::PARAM_NUMBER_OF_RESTARTS, "Количество перезапусков", false, true, 255);
	appendParam(zrm::PARAM_TIMEOUT_OF_RESTARTS, "Таймаут перезапуска", false, true, 600000);
	appendParam(zrm::PARAM_PROTECTION_RESPONSE_LIMIT, "Предел срабатывания защиты", false, true, 255);
	appendParam(zrm::PARAM_LOAD_BREAKAGE_MONITORING, "Отключить контроль обрыва нагрузки", false, true, 1);
	appendParam(zrm::PARAM_LOAD_BREAK_TIMEOUT, "Таймаут обрыва нагрузки", false, true, 60000);
    appendParam(zrm::PARAM_ADDRESS_EDIT, "Адрес блока", false, true, 255);

	gbWriteParams->setVisible(false);
}

QString ZrmParamsView::getCurrentStateText(quint32 stValue)
{
	using TextMap = QMap<quint32, QString>;
	static  TextMap texts =
	{
		{0, QObject::tr("ЗМ и РМ остановлены")},
		{1, QObject::tr("Запуск заряда/разряда")},
		{2, QObject::tr("Выравниваение напряжения")},
		{3, QObject::tr("Замыкание реле")},
		{4, QObject::tr("Включить преобразователь")},
		{5, QObject::tr("Выклчюение преобразователя")},
		{6, QObject::tr("Включить МЭН")},
		{7, QObject::tr("Ожидание включения МЭН")},
		{8, QObject::tr("Выключить МЭН")},
		{9, QObject::tr("Ожидание спада тока")},
		{10, QObject::tr("Размыкание реле")},
		{11, QObject::tr("Пауза")},
		{12, QObject::tr("Рабочий цикл заряда/разряда")},
		{13, QObject::tr("Перезапуск при перегрузке")},
		{14, QObject::tr("Плавный старт SDC")}
	};

	TextMap::iterator ptr = texts.find(stValue);
	if (ptr == texts.end())
		return QObject::tr("Неизвестный состояние");

	return ptr.value();
}

void ZrmParamsView::channel_param_changed(unsigned channel, const zrm::params_list_t& params_list)
{
	if (m_source && m_channel == channel)
	{
		setUpdatesEnabled(false);
		for (auto param : params_list)
		{
			params_items_t::iterator item = items.find(zrm::zrm_param_t(param.first));

			if (item != items.end())
			{
				QString str = (param.first == zrm::PARAM_ST) ?
							  getCurrentStateText(ZrmParamCvt::toUint32(param.second).toUInt())
							  :
							  ZrmParamCvt::toVariant(param.first, param.second).toString()
							  ;
				item.value()->setText(column_value, str);

			}
		}
		setUpdatesEnabled(true);
	}
	ZrmChannelWidget::channel_param_changed(channel, params_list);
}

void    ZrmParamsView::update_controls()
{
	ZrmChannelWidget::update_controls();
	if (m_source && m_channel)
		channel_param_changed(m_channel, m_source->channel_params(m_channel));
}

void    ZrmParamsView::clear_controls()
{
	for (auto&& item : items)
	{
		item->setText(column_value, QString());
		item->setText(column_new_value, QString());
	}
	if (respond)
		respond->setText(column_value, QString());
}

void ZrmParamsView::onActivate()
{
	ZrmChannelWidget::onActivate();
	qDebug() << Q_FUNC_INFO;
	if (m_source && m_source->channel_session(m_channel).is_active())
	{
		requestTimer.start(std::chrono::milliseconds(333));
		m_source->channel_query_params(m_channel, queryParms);
		request();
		QTimer::singleShot(0, this, &ZrmParamsView::update_controls);
	}
}

void ZrmParamsView::onDeactivate()
{
	ZrmChannelWidget::onDeactivate();
	requestTimer.stop();
}

void ZrmParamsView::on_connected (bool con_state)
{
	ZrmChannelWidget::on_connected(con_state);
	if (!con_state)
	{
		onDeactivate();
	}
	else
	{
		if (isVisible())
			onActivate();
	}
}

void    ZrmParamsView::request()
{
	if (m_source && m_channel)
	{
		m_source->channel_query_params(m_channel, orders);
		if (respond)
		{
			qint64  tm = m_source->channelRespondTime(m_channel);
			respond->setText(column_value, tm ?  QString("%1 ms").arg( tm ) : QString()) ;
		}
	}
}

void ZrmParamsView::serviceMode()
{
	if (m_source && m_channel && m_source->channel_session(m_channel).is_active())
	{
		if (m_source && m_channel && QMessageBox::Yes != QMessageBox::question(this, "Сервисный режим", "Перевести устройство в сервисный режим ?"))
			return;
		quint8 wr_value = 0xAA;
		m_source->channel_write_param(m_channel, zrm::WM_PROCESS_AND_WRITE, zrm::PARAM_BOOT_LOADER, &wr_value, sizeof(wr_value));
	}
	else
		QMessageBox::information(this, tr("Внимание!"), tr("Нет канала для передачи"));
}

void ZrmParamsView::synchronizationClock()
{
	if (m_source && m_channel && m_source->channel_session(m_channel).is_active())
	{
		if (QMessageBox::Yes != QMessageBox::question(this, "Синхронизация времени", "Передать текущее время на устройство ?"))
			return;
		QDateTime dt = QDateTime::currentDateTime();
		zrm::sync_time_t st;
		st.Year = dt.date().year();
		st.Month = dt.date().month();
		st.Day = dt.date().day();
		st.Hour = dt.time().hour();
		st.Min = dt.time().minute();
		st.Sec = dt.time().second();
		st.mSec = dt.time().msec();
		m_source->channel_write_param(m_channel, zrm::WM_PROCESS_AND_WRITE, zrm::PARAM_SYNCHRONIZATION_CLOCK, &st, sizeof(st));
	}
	else
		QMessageBox::information(this, tr("Внимание!"), tr("Нет канала для передачи"));
}

void ZrmParamsView::paramChanged(QTreeWidgetItem*, int column)
{
	if (column != column_new_value)
		return;

	for (const QTreeWidgetItem* testItem : qAsConst(editableItems))
	{
		if (testItem->text(column_new_value).trimmed().isEmpty())
			continue;
		gbWriteParams->setVisible(true);
		return;
	}
	gbWriteParams->setVisible(false);
}

bool ZrmParamsView::writeParameter(zrm::zrm_param_t param, const QString& value)
{
	bool ok = false;
	qlonglong longValue = value.toLong(&ok);
	if (!ok)
		return ok;

	switch (param)
	{
		case zrm::PARAM_NUMBER_OF_RESTARTS :    // изменяем количество перезапусков
		case zrm::PARAM_TIMEOUT_OF_RESTARTS :   // изменяем таймаут перезапуска
		case zrm::PARAM_LOAD_BREAK_TIMEOUT :    // изменяем таймаут обрыва нагрузки
		{
			uint32_t valueWrite = longValue;
			if (m_source && m_channel && m_source->channel_session(m_channel).is_active())
				m_source->channel_write_param(m_channel, zrm::WM_PROCESS_AND_WRITE, param, &valueWrite, sizeof(valueWrite));
		}
		break;
		case zrm::PARAM_PROTECTION_RESPONSE_LIMIT : // изменяем предел срабатывания защиты
		case zrm::PARAM_LOAD_BREAKAGE_MONITORING :  // отключаем контроль обрыва нагрузки
        case zrm::PARAM_ADDRESS_EDIT :  // редактируем адрес блока
		{
			uint8_t valueWrite = longValue;
			if (m_source && m_channel && m_source->channel_session(m_channel).is_active())
				m_source->channel_write_param(m_channel, zrm::WM_PROCESS_AND_WRITE, param, &valueWrite, sizeof(valueWrite));
		}
		break;
		default :
			ok = false;
			break;
	}
	return ok;

	/*bool ok = false;
	if (m_channel)
	{
		zrm::param_variant pv = param_get(param);
		if (pv.size)
		{
			int newValue = value.toInt(&ok);
			if (ok)
			{
				m_source->channel_write_param(m_channel, zrm::param_write_mode_t::WM_PROCESS_AND_WRITE, param, &newValue, pv.size);
			}
		}
	}
	return ok;*/
}

void ZrmParamsView::writeParameters()
{
	for (QTreeWidgetItem* item : qAsConst(editableItems))
	{
		QString newValue = item->text(column_new_value).trimmed();
		if (newValue.isEmpty())
			continue;
		zrm::zrm_param_t param = items.key(item);
		qDebug() << "param is " << quint32(param);
		if (writeParameter(param, newValue))
		{
			item->setText(column_value, newValue);
			item->setText(column_new_value, QString());
		}
		else
		{
			zrm::param_variant pv = param_get(param);
			item->setText(column_new_value, pv.size ? "Error convert" : "Not exists");
		}
	}
	passwd->clear();
}

void ZrmParamsView::undoParameters()
{
	for (QTreeWidgetItem* item : qAsConst(editableItems))
	{
		item->setText(column_new_value, QString());
	}
}

void ZrmParamsView::passwdChanged(const QString& text)
{
	actWriteParameters->setDisabled(text.trimmed().isEmpty());
}

