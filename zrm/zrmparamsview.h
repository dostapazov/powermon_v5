﻿#ifndef ZRMPARAMSVIEW_H
#define ZRMPARAMSVIEW_H

#include "ui_zrmparamsview.h"
#include <zrmbasewidget.h>
#include <qmap.h>
#include <QIcon>



class ZrmParamsView : public ZrmChannelWidget, private Ui::ZrmParamsView
{
	Q_OBJECT
public:
	enum     columns_t : int {column_name, column_value, column_new_value};

	explicit ZrmParamsView(QWidget* parent = nullptr);
	void    channel_param_changed(unsigned channel, const zrm::params_list_t& params_list  ) override;
	void    update_controls() override;
	void    clear_controls() override;
	void initSlotConnection();

private slots:
	void request ();
	void serviceMode();
	void synchronizationClock();
	void writeParameters();
	void undoParameters();
	void paramChanged(QTreeWidgetItem*, int column);
	void passwdChanged(const QString& text);

private:
	void init_params();
	bool writeParameter(zrm::zrm_param_t param, const QString& value);
	void appendParam(zrm::zrm_param_t, const QString& text, bool ordered, bool editable = false, int max = 255);
	using params_items_t  =  QMap<zrm::zrm_param_t, QTreeWidgetItem* >;
	void onActivate() override;
	void onDeactivate() override;
	void on_connected (bool con_state) override;
	static QString getCurrentStateText(quint32 stValue);

	zrm::params_t    orders;
	zrm::params_t    queryParms;
	params_items_t   items;
	QList<QTreeWidgetItem*> editableItems;
	QTreeWidgetItem* respond = nullptr;
	QTimer           requestTimer;
	QIcon            editableIcon;

};

#endif // ZRMPARAMSVIEW_H
