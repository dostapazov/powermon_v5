﻿#include "zrmreport.h"

#include "ZrmLogerReportChart.h"
#include "xlsxdocument.h"

#include <qfiledialog.h>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDesktopServices>
#include <QtPrintSupport/qprinter.h>

ZrmReport::ZrmReport(QWidget* parent) :
	ZrmChannelWidget(parent)
{
	setupUi(this);

	requestTimer.setInterval(std::chrono::milliseconds(1000));
	connect(&requestTimer, &QTimer::timeout, this, [this] {channel_session(m_channel);});

	connect(tbSaveHtml, &QAbstractButton::clicked, this, &ZrmReport::save_report);
	connect(tbSavePdf, &QAbstractButton::clicked, this, &ZrmReport::save_report);
	connect(tbSaveXlsx, &QAbstractButton::clicked, this, &ZrmReport::save_report);
	connect(tbSaveSql, &QAbstractButton::clicked, this, &ZrmReport::save_report_sql);
	connect(cb_report_details, &QAbstractButton::clicked, this, &ZrmReport::gen_result_report);
	connect(ed_report_maker, &QComboBox::currentTextChanged, this, &ZrmReport::gen_result_report);
	connect(ed_akb_type, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &ZrmReport::akb_type_changed);
	connect(ed_akb_number, &QComboBox::currentTextChanged, this, &ZrmReport::gen_result_report);
	connect(&timerDetails, &QTimer::timeout, this, &ZrmReport::getDetails);
}

QString ZrmReport::make_report(const QString& a_maker_name, const QString& a_akb_type, const QString& a_akb_number, const zrm::method_exec_results_t& results)
{
	results_last = results;
	QString result;
	int32_t total_duration = 0;
	double total_capacity = 0;
	double total_energy = 0;

	QStringList  main_text ;
	maker_name = a_maker_name.isEmpty() ? tr("_________") : a_maker_name;
	akb_type   = a_akb_type.isEmpty  () ? tr("_________") : a_akb_type;
	akb_number = a_akb_number.isEmpty() ? tr("_________") : a_akb_number;

	QStringList  details_table;
	details_table += QString("<table width=600 border=1><caption><h3>%1</h3></caption> ").arg(tr("Результаты этапов"));
	details_table +=
		QString("<tr align=center><td>%1</td> <td>%2</td> <td>%3</td> <td>%4</td> <td>%5</td> <td>%6</td> <td>%7</td></tr> ")
		.arg(tr("Этап" ), tr("I нач"), tr("I кон"), tr("U нач"), tr("U кон"), tr("Ёмкость"), tr("Время"));


	QString detail_row = tr("<tr align=center><td>%1</td> "
							"<td style=\"text-align: right;\">%2 A</td> "
							"<td style=\"text-align: right;\">%3 А</td> "
							"<td style=\"text-align: right;\">%4 В</td> "
							"<td style=\"text-align: right;\">%5 В</td> "
							"<td style=\"text-align: right;\">%6 А*Ч</td>"
							"<td>%7</td></tr>");
	for (auto res : results)
	{
		qreal CAP  = qreal(res.capcacity ) / 1000.0;
		total_duration += uint32_t(res.duration[0]) * 3600 + uint32_t(res.duration[1]) * 60 + uint32_t(res.duration[2]);
		total_energy   += CAP;
		if (res.state & zrm::stage_end_flags_and_t::stage_capacity_measure)
			total_capacity = qMax(total_capacity, fabs(CAP));

		qreal Ibeg = qreal(res.curr_begin) / 1000.0;
		qreal Iend = qreal(res.curr_end  ) / 1000.0;
		qreal Ubeg = qreal(res.volt_begin) / 1000.0;
		qreal Uend = qreal(res.volt_end  ) / 1000.0;

		details_table += detail_row.arg(uint32_t(res.stage)).arg(Ibeg, 0, 'f', 2).arg(Iend, 0, 'f', 2)
						 .arg(Ubeg, 0, 'f', 2).arg(Uend, 0, 'f', 2).arg(CAP, 0, 'f', 2)
						 .arg(tr("%1:%2:%3").arg(res.duration[0], 2, 10, QLatin1Char('0'))
							  .arg(res.duration[1], 2, 10, QLatin1Char('0'))
							  .arg(res.duration[2], 2, 10, QLatin1Char('0'))
							 );
	}

	details_table += tr("</table>\n");

	// data control
	mapUBeg.clear();
	mapUEnd.clear();
	mapUMax.clear();
	mapUCrit.clear();
	mapC.clear();
	mapTime.clear();
	rowDetailsControl = 0;

	auto getParam = [this](zrm::zrm_param_t param, QMap<uint8_t, int16_t>& mapData)
	{
		zrm::param_variant paramVariant = param_get(param);
		int count = paramVariant.size / 3;
		for (int i = 0; i < count; i++)
		{
			uint8_t number;
			int16_t value;
			memcpy(&number, paramVariant.pchar + i * 3, 1);
			memcpy(&value, paramVariant.pchar + i * 3 + 1, 2);
			mapData[number] = value;
			if (number > rowDetailsControl)
				rowDetailsControl = number;
		}
	};
	getParam(zrm::PARAM_METH_EXEC_U_BEGIN, mapUBeg);
	getParam(zrm::PARAM_METH_EXEC_U_END, mapUEnd);
	getParam(zrm::PARAM_METH_EXEC_U_MAX, mapUMax);
	getParam(zrm::PARAM_METH_EXEC_U_CRIT, mapUCrit);

	// вычисляем минимальную ёмкость
	uint32_t minCUInt;
	zrm::param_variant paramVariantC = param_get(zrm::PARAM_METH_EXEC_C);
	int count = paramVariantC.size / 5;
	if (count > 0)
	{
		int32_t value;
		memcpy(&value, paramVariantC.pchar + 1, 4);
		minCUInt = abs(value);
	}

	for (int i = 0; i < count; i++)
	{
		uint8_t number;
		int32_t value;
		memcpy(&number, paramVariantC.pchar + i * 5, 1);
		memcpy(&value, paramVariantC.pchar + i * 5 + 1, 4);
		mapC[number] = value;
		if (number > rowDetailsControl)
			rowDetailsControl = number;
		minCUInt = std::min(minCUInt, uint32_t(abs(value)));
	}

	if (count > 0)
		minC = (double)minCUInt / 1000.;
	else
		minC = total_capacity;

	zrm::param_variant paramVariantT = param_get(zrm::PARAM_METH_EXEC_TIME);
	count = paramVariantT.size / 3;
	for (int i = 0; i < count; i++)
	{
		uint8_t number;
		uint16_t value;
		memcpy(&number, paramVariantT.pchar + i * 3, 1);
		memcpy(&value, paramVariantT.pchar + i * 3 + 1, 2);
		mapTime[number] = value;
		if (number > rowDetailsControl)
			rowDetailsControl = number;
	}

	details_table += QString("<table width=600 border=1><caption><h3>%1</h3></caption> ").arg(tr("Отчёт поаккумуляторного контроля"));
	details_table +=
		QString("<tr align=center><td>%1</td> <td>%2</td> <td>%3</td> <td>%4</td> <td>%5</td> <td>%6</td> <td>%7</td></tr> ")
		.arg(tr("№" ), tr("U нач"), tr("U кон"), tr("U макс"), tr("U крит"), tr("Ёмкость"), tr("Время, мин"));

	detail_row = tr("<tr align=center><td>%1</td> "
					"<td style=\"text-align: right;\">%2</td> "
					"<td style=\"text-align: right;\">%3</td> "
					"<td style=\"text-align: right;\">%4</td> "
					"<td style=\"text-align: right;\">%5</td> "
					"<td style=\"text-align: right;\">%6</td> "
					"<td>%7</td></tr>");

	for (int i = 1; i <= rowDetailsControl; i++)
	{
		details_table += detail_row
						 .arg(i)
						 .arg(mapUBeg.contains(i) ? QString::number(double(mapUBeg[i]) / 1000., 'f', 2) : "")
						 .arg(mapUEnd.contains(i) ? QString::number(double(mapUEnd[i]) / 1000., 'f', 2) : "")
						 .arg(mapUMax.contains(i) ? QString::number(double(mapUMax[i]) / 1000., 'f', 2) : "")
						 .arg(mapUCrit.contains(i) ? QString::number(double(mapUCrit[i]) / 1000., 'f', 2) : "")
						 .arg(mapC.contains(i) ? QString::number(double(mapC[i]) / 1000., 'f', 2) : "")
						 .arg(mapTime.contains(i) ? QString::number(mapTime[i]) : "");
	}

	details_table += tr("</table>\n");

	main_text += tr("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\" http://www.w3.org\">");
	main_text += tr("<html><head>");
	main_text += tr("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
	main_text += tr("<title>%1</title>").arg(QDateTime::currentDateTime().toString("dd-MM-yyyy hh:mm:ss"));
	main_text += tr("<style type=\"text/css\"> td { white-space: nowrap; } </style>");
	main_text += tr("</head><body><header><h2>%1</h2></header>").arg("Отчет об обслуживании АКБ");
	main_text += tr("<p style=\"white-space: pre-wrap;\">Тип АКБ    %1 № %2      Ответственный %3<br>").arg(akb_type, akb_number, maker_name);
	zrm::param_variant paramNameTime = param_get(zrm::PARAM_METH_EXEC_NAME_TIME);
	dtStat = QDateTime();
	dtEnd = QDateTime();
	zrm::sync_time_t dateStart, dateEnd;
	memcpy(&dateStart, paramNameTime.pchar + 33, 7);
	memcpy(&dateEnd, paramNameTime.pchar + 40, 7);
	dtStat.setDate(QDate(dateStart.Year, dateStart.Month, dateStart.Day));
	dtStat.setTime(QTime(dateStart.Hour, dateStart.Min, dateStart.Sec));
	dtEnd.setDate(QDate(dateEnd.Year, dateEnd.Month, dateEnd.Day));
	dtEnd.setTime(QTime(dateEnd.Hour, dateEnd.Min, dateEnd.Sec));
	main_text += tr("Дата начало: %1; завершение: %2<br>")
				 .arg(dtStat.toString("yyyy-MM-dd hh:mm:ss"))
				 .arg(dtEnd.toString("yyyy-MM-dd hh:mm:ss"));
	auto hms = pwm_utils::secunds2hms(uint32_t(total_duration));
	main_text += tr("Время выполнения %1:%2:%3<br>")
				 .arg(std::get<0>(hms), 2, 10, QChar('0'))
				 .arg(std::get<1>(hms), 2, 10, QChar('0'))
				 .arg(std::get<2>(hms), 2, 10, QChar('0'));
	methodName.clear();
	char name[zrm::METHOD_NAME_SIZE];
	memcpy(name, paramNameTime.pchar, zrm::METHOD_NAME_SIZE);
	methodName = to_utf(name, sizeof(name)).trimmed();
	main_text += tr("Профиль (метод): %1<br>").arg(methodName);

	main_text += tr("%1 %2 А*Ч<br>").arg(total_energy < 0 ? tr("Из АКБ потреблено") : tr("В АКБ передано"))
				 .arg(fabs(total_energy), 0, 'f', 2);

	if (!qFuzzyIsNull(total_capacity))
		main_text += tr("Ёмкость АКБ %1 А*Ч      Минимальная ёмкость АКБ %2 А<br>").arg(total_capacity, 0, 'f', 2).arg(minC, 0, 'f', 2);

	int resSize = results.size();
	double Ubeg = 0., Uend = 0., Iend = 0.;
	if (resSize > 0)
	{
		Ubeg = double(results[0].volt_begin) / 1000.0;
		Uend = double(results[resSize - 1].volt_end) / 1000.0;
		Iend = double(results[resSize - 1].curr_end) / 1000.0;
	}
	main_text += tr("Напряжение начальное %1 В; конечное %2 В; ток %3 А<br>").arg(Ubeg, 0, 'f', 2).arg(Uend, 0, 'f', 2).arg(Iend, 0, 'f', 2);
	main_text += tr("</p>\n");

	main_text.append(details_table);
	main_text += tr("</body></html>");
	result = main_text.join(QChar('\n'));
	return result;
}

void ZrmReport::save_report_html(const QString& file_name)
{
	QFile file(file_name);
	if (result_text && file.open(QFile::WriteOnly | QFile::Truncate))
	{
		file.write(result_text->toHtml().toUtf8());
	}

}

void ZrmReport::save_report_pdf (const QString& file_name)
{
	QPrinter printer(QPrinter::ScreenResolution);
	printer.setOutputFormat(QPrinter::PdfFormat);

#if QT_VERSION < QT_VERSION_CHECK(5,15,0)
	printer.setOrientation(cb_report_details->isChecked() ? QPrinter::Orientation::Landscape : QPrinter::Orientation::Portrait);
#else
	printer.setPageOrientation(cb_report_details->isChecked() ? QPageLayout::Landscape : QPageLayout::Portrait);
#endif
	printer.setOutputFileName(file_name);
	//result_text->print(&printer);

	QTextEdit te(result_text->toHtml());
	if (cb_report_details->isChecked())
	{
		// отрисовка графика
		QTextDocument* doc = te.document();
		int maxw = logerChart->maximumWidth();
		int maxh = logerChart->maximumHeight();
		int minw = logerChart->minimumWidth();
		int minh = logerChart->minimumHeight();
		logerChart->setMaximumWidth(printer.width() - 50);
		logerChart->setMaximumHeight(printer.height() - 150);
		logerChart->setMinimumWidth(printer.width() - 50);
		logerChart->setMinimumHeight(printer.height() - 150);
		QPixmap pix = logerChart->grabPixmap();
		logerChart->setMaximumWidth(maxw);
		logerChart->setMaximumHeight(maxh);
		logerChart->setMinimumWidth(minw);
		logerChart->setMinimumHeight(minh);
		QImage image(pix.toImage());
		doc->addResource(QTextDocument::ImageResource, QUrl("image"), image);
		QTextCursor cursor = te.textCursor();
		cursor.movePosition(QTextCursor::End);
		cursor.insertImage("image");
	}
	te.print(&printer);
}

void ZrmReport::save_report_xlsx(const QString& file_name)
{
	QXlsx::Document xlsx;
	int rowNum = 1;

	QFont f = result_text->font();
	f.setBold(true);
	QXlsx::Format headerFmt;
	headerFmt.setFont(f);
	QXlsx::Format tableFmt;
	tableFmt.setFont(f);
	tableFmt.setBorderStyle(QXlsx::Format::BorderMedium);
	QFontMetrics fm(f);
	int chWidth = fm.horizontalAdvance('Q') / 2;

	int32_t total_duration = 0;
	double total_capacity = 0;
	double total_energy = 0;
	for (auto res : results_last)
	{
		qreal CAP  = qreal(res.capcacity ) / 1000.0;
		total_duration += uint32_t(res.duration[0]) * 3600 + uint32_t(res.duration[1]) * 60 + uint32_t(res.duration[2]);
		total_energy   += CAP;
		if (res.state & zrm::stage_end_flags_and_t::stage_capacity_measure)
			total_capacity = qMax(total_capacity, fabs(CAP));
	}

	xlsx.write(rowNum++, 1, tr("Отчет об обслуживании АКБ"), headerFmt);
	rowNum++;

	xlsx.write(rowNum++, 1, QString("Тип АКБ    %1 № %2      Ответственный %3").arg(akb_type, akb_number, maker_name), headerFmt);
	xlsx.write(rowNum++, 1, QString("Дата начало: %1; завершение: %2")
			   .arg(dtStat.toString("yyyy-MM-dd hh:mm:ss"))
			   .arg(dtEnd.toString("yyyy-MM-dd hh:mm:ss")), headerFmt);
	auto hms = pwm_utils::secunds2hms(uint32_t(total_duration));
	xlsx.write(rowNum++, 1, tr("Время выполнения %1:%2:%3")
			   .arg(std::get<0>(hms), 2, 10, QChar('0'))
			   .arg(std::get<1>(hms), 2, 10, QChar('0'))
			   .arg(std::get<2>(hms), 2, 10, QChar('0')), headerFmt);
	xlsx.write(rowNum++, 1, tr("Профиль (метод): %1").arg(methodName), headerFmt);
	xlsx.write(rowNum++, 1, tr("%1 %2 А*Ч").arg(total_energy < 0 ? tr("Из АКБ потреблено") : tr("В АКБ передано")).arg(fabs(total_energy), 0, 'f', 2), headerFmt);
	if (!qFuzzyIsNull(total_capacity))
		xlsx.write(rowNum++, 1, tr("Ёмкость АКБ %1 А*Ч      Минимальная ёмкость АКБ %2 А").arg(total_capacity, 0, 'f', 2).arg(minC, 0, 'f', 2), headerFmt);
	int resSize = results_last.size();
	double Ubeg = 0., Uend = 0., Iend = 0.;
	if (resSize > 0)
	{
		Ubeg = double(results_last[0].volt_begin) / 1000.0;
		Uend = double(results_last[resSize - 1].volt_end) / 1000.0;
		Iend = double(results_last[resSize - 1].curr_end) / 1000.0;
	}
	xlsx.write(rowNum++, 1, QString("Напряжение начальное %1 В; конечное %2 В; ток %3 А").arg(Ubeg, 0, 'f', 2).arg(Uend, 0, 'f', 2).arg(Iend, 0, 'f', 2), headerFmt);
	rowNum++;

	xlsx.write(rowNum++, 1, "Результаты этапов", headerFmt);
	int col = 1;
	xlsx.write(rowNum, col++, tr("Этап"), tableFmt);
	xlsx.write(rowNum, col++, tr("I нач"), tableFmt);
	xlsx.write(rowNum, col++, tr("I кон"), tableFmt);
	xlsx.write(rowNum, col++, tr("U нач"), tableFmt);
	xlsx.write(rowNum, col++, tr("U кон"), tableFmt);
	xlsx.write(rowNum, col, tr("Ёмкость"), tableFmt);
	xlsx.setColumnWidth(col++, fm.horizontalAdvance(tr("Ёмкость")) / chWidth);
	xlsx.write(rowNum, col, "Время", tableFmt);
	xlsx.setColumnWidth(col++, fm.horizontalAdvance(tr("Время, мин")) / chWidth);
	rowNum++;

	for (auto res : results_last)
	{
		qreal CAP  = qreal(res.capcacity ) / 1000.0;
		qreal Ibeg = qreal(res.curr_begin) / 1000.0;
		qreal Iend = qreal(res.curr_end  ) / 1000.0;
		qreal Ubeg = qreal(res.volt_begin) / 1000.0;
		qreal Uend = qreal(res.volt_end  ) / 1000.0;

		col = 1;
		xlsx.write(rowNum, col++, uint32_t(res.stage), tableFmt);
		xlsx.write(rowNum, col++, QString::number(Ibeg, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString::number(Iend, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString::number(Ubeg, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString::number(Uend, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString::number(CAP, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString("%1:%2:%3").arg(res.duration[0], 2, 10, QLatin1Char('0'))
				   .arg(res.duration[1], 2, 10, QLatin1Char('0'))
				   .arg(res.duration[2], 2, 10, QLatin1Char('0')), tableFmt);
		rowNum++;
	}
	rowNum++;

	xlsx.write(rowNum++, 1, "Отчёт поаккумуляторного контроля", headerFmt);
	col = 1;
	xlsx.write(rowNum, col++, "№", tableFmt);
	xlsx.write(rowNum, col++, tr("U нач"), tableFmt);
	xlsx.write(rowNum, col++, tr("U кон"), tableFmt);
	xlsx.write(rowNum, col++, tr("U макс"), tableFmt);
	xlsx.write(rowNum, col++, tr("U крит"), tableFmt);
	xlsx.write(rowNum, col++, tr("Ёмкость"), tableFmt);
	xlsx.write(rowNum, col++, tr("Время, мин"), tableFmt);
	rowNum++;

	for (int i = 1; i <= rowDetailsControl; i++)
	{
		col = 1;
		xlsx.write(rowNum, col++, i, tableFmt);
		xlsx.write(rowNum, col++, mapUBeg.contains(i) ? QString::number(double(mapUBeg[i]) / 1000., 'f', 2) : "", tableFmt);
		xlsx.write(rowNum, col++, mapUEnd.contains(i) ? QString::number(double(mapUEnd[i]) / 1000., 'f', 2) : "", tableFmt);
		xlsx.write(rowNum, col++, mapUMax.contains(i) ? QString::number(double(mapUMax[i]) / 1000., 'f', 2) : "", tableFmt);
		xlsx.write(rowNum, col++, mapUCrit.contains(i) ? QString::number(double(mapUCrit[i]) / 1000., 'f', 2) : "", tableFmt);
		xlsx.write(rowNum, col++, mapC.contains(i) ? QString::number(double(mapC[i]) / 1000., 'f', 2) : "", tableFmt);
		xlsx.write(rowNum, col++, mapTime.contains(i) ? QString::number(mapTime[i]) : "", tableFmt);
		rowNum++;
	}

	xlsx.saveAs(file_name);
}

void ZrmReport::save_report()
{
	QString doc_dir  ;
	QString file_name;
#ifdef Q_OS_ANDROID
	doc_dir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
#else
	doc_dir = qApp->applicationDirPath();
#endif
	QString filter;
	QObject* s = sender();
	if (tbSaveHtml == s)
		filter = tr("HTML (*.html *.htm)");
	if (tbSavePdf == s)
		filter = tr("PDF (*.pdf)");
	if (tbSaveXlsx == s)
		filter = tr("Файл EXCEL (*.xlsx)");

	file_name = QFileDialog::getSaveFileName(this, tr("Сохранение результатов"), doc_dir, filter);
	if (!file_name.isEmpty())
	{
		if (tbSaveHtml == s)
			save_report_html(file_name);
		if (tbSavePdf == s)
			save_report_pdf(file_name);
		if (tbSaveXlsx == s)
			save_report_xlsx(file_name);

		QUrl url = QUrl::fromLocalFile(file_name);
		QDesktopServices::openUrl(url);
	}
}

void ZrmReport::gen_result_report()
{
	requestTimer.stop();
	m_method_result = m_source->results_get(m_channel);
	bool report_valid = m_method_result.size() && !currentState.is_executing();
	//Обновить  список номеров при необходимости
	if (report_valid && !ed_akb_number->count() && ed_akb_type->count())
		akb_type_changed(ed_akb_type->currentIndex());

	tbSaveHtml->setEnabled(report_valid);
	tbSavePdf->setEnabled (report_valid);
	tbSaveXlsx->setEnabled(report_valid);
	tbSaveSql->setEnabled (report_valid && ed_report_maker->currentIndex() >= 0 && ed_akb_number->currentIndex() >= 0);
	if (report_valid)
	{
		QString rep_maker = ed_report_maker->currentText();
		QString rep_text = make_report(rep_maker, ed_akb_type->currentText(), ed_akb_number->currentText(), m_method_result);
		result_text->setText(rep_text);
		result_text->moveCursor(QTextCursor::MoveOperation::Start);
	}
	else
	{
		result_text->clear();
		if (currentState.is_executing())
			result_text->setText("Отчет во время выполнения метода недоступен");
		requestTimer.start();
	}
}

void ZrmReport::update_controls()
{

	if (m_source && m_channel)
	{

		if (ed_report_maker->model() != m_rep_db.users_model())
		{
			ed_report_maker->setModel      ( m_rep_db.users_model()     );
			ed_report_maker->setModelColumn( m_rep_db.users_short_fio() );
			ed_akb_type->setModel          ( m_rep_db.types_model()     );
			ed_akb_type->setModelColumn    ( m_rep_db.type_text()       );
			ed_akb_number->setModel        ( m_rep_db.numbers_model()   );
			ed_akb_number->setModelColumn  ( m_rep_db.number_text()     );

			ed_report_maker->setCurrentIndex(-1);
			ed_akb_type->setCurrentIndex(-1);
			ed_akb_number->setCurrentIndex(-1);
		}

		setEnabled(true);
		channel_param_changed(m_channel, m_source->channel_params(m_channel));
#ifdef QT_DEBUG
		tbSaveSql->setEnabled(true);
#endif
	}
	ZrmChannelWidget::update_controls();
}

void ZrmReport::clear_controls ()
{
	setEnabled(false);
	requestTimer.stop();
	result_text->clear();

	tbSaveHtml->setEnabled(false);
	tbSavePdf->setEnabled(false);
	tbSaveXlsx->setEnabled(false);
}

void ZrmReport::channel_param_changed(unsigned channel, const zrm::params_list_t& params_list)
{
	if (channel == m_channel)
	{
		for (auto param : params_list)
		{
			switch (param.first)
			{
				case zrm::PARAM_STATE        :
					onState(param.second.value<uint32_t>(false));
					break;

				case zrm::PARAM_METH_EXEC_RESULT :
				{
					//qDebug() << "Handle results " << param.first;
					timerDetails.stop();
					gen_result_report();
					getDetails();
				}
				break;

				case zrm::PARAM_METH_EXEC_NAME_TIME :
				case zrm::PARAM_METH_EXEC_U_BEGIN :
				case zrm::PARAM_METH_EXEC_U_END :
				case zrm::PARAM_METH_EXEC_U_MAX :
				case zrm::PARAM_METH_EXEC_U_CRIT :
				case zrm::PARAM_METH_EXEC_C :
				case zrm::PARAM_METH_EXEC_TIME :
				{
					timerDetails.stop();
					gen_result_report();
				}
				break;

				case zrm::PARAM_DATA_SAVE :
				{
					uint size = sizeof (qlonglong);
					if (param.second.size >= size)
					{
						// номер устройства
						qlonglong idAKB = param.second.sqword;
						if (idAKB > 0)
						{
							QString query_text = QString("SELECT id_type FROM tbattery_list WHERE id = %1 ").arg(idAKB);
							QSqlQuery query(*m_rep_db.database());
							query.prepare(query_text);
							query.exec();
							query.next();
							QVariant idType = query.value(0);
							QSqlTableModel* tm = m_rep_db.types_model();
							for (int i = 0; i < tm->rowCount(); i++)
							{
								QSqlRecord rec = tm->record(i);
								if (rec.value("id") == idType)
								{
									ed_akb_type->setCurrentIndex(i);
									m_rep_db.numbers_select(idType);
									QSqlTableModel* nm = m_rep_db.numbers_model();
									for (int j = 0; j < nm->rowCount(); j++)
									{
										QSqlRecord rec = nm->record(j);
										if (rec.value("id").toLongLong() == idAKB)
										{
											ed_akb_number->setCurrentIndex(j);
											break;
										}
									}
									break;
								}
							}
						}
						if (param.second.size >= 2 * size)
						{
							// id пользователя
							qlonglong idUser;
							memcpy(&idUser, param.second.pchar + size, size);
							if (idUser > 0)
							{
								QSqlTableModel* um = m_rep_db.users_model();
								for (int i = 0; i < um->rowCount(); i++)
								{
									QSqlRecord rec = um->record(i);
									if (rec.value("id").toLongLong() == idUser)
									{
										ed_report_maker->setCurrentIndex(i);
										break;
									}
								}
							}
						}
					}
				}
				break;

				default:
				{
				}
				break;
			}
		}
	}
	ZrmChannelWidget::channel_param_changed(channel, params_list);
}

void ZrmReport::getDetails()
{
	if (m_source && m_channel && m_source->channel_session(m_channel).is_active())
	{
		static quint8 count = 10;

		zrm::params_t params;
		params.push_back(zrm::PARAM_METH_EXEC_NAME_TIME);
		params.push_back(zrm::PARAM_METH_EXEC_U_BEGIN);
		params.push_back(zrm::PARAM_METH_EXEC_U_END);
		params.push_back(zrm::PARAM_METH_EXEC_U_MAX);
		params.push_back(zrm::PARAM_METH_EXEC_U_CRIT);
		params.push_back(zrm::PARAM_METH_EXEC_C);
		params.push_back(zrm::PARAM_METH_EXEC_TIME);
		params.push_back(zrm::PARAM_DATA_SAVE);
		m_source->channel_query_params(m_channel, params);

		if (!timerDetails.isActive())
		{
			count = 10;
			timerDetails.start(1000);
		}
		count--;
		if (0 == count)
			timerDetails.stop();
	}
	else
		timerDetails.stop();
}

void ZrmReport::channel_session      (unsigned ch_num)
{
	//qDebug() << Q_FUNC_INFO;
	if (m_source && m_channel == ch_num && m_source->channel_session(m_channel).is_active())
	{
		//qDebug() << " query results";
		m_source->channel_query_param(m_channel, zrm::PARAM_METH_EXEC_RESULT);
	}

}

void ZrmReport::onActivate()
{
	ZrmChannelWidget::onActivate();
	if (m_source && m_source->channel_session(m_channel).is_active())
	{
		requestTimer.start();
		m_source->channel_query_param(m_channel, zrm::PARAM_METH_EXEC_RESULT);
	}
}

void ZrmReport::akb_type_changed (int idx)
{
	QAbstractItemModel* model = ed_akb_type->model();
	int type_id = m_rep_db.get_record_id(model, idx);
	if (m_rep_db.numbers_select( type_id ) && ed_akb_number->count() )
		ed_akb_number->setCurrentIndex(0);

}

void ZrmReport::save_report_sql()
{
#ifdef QT_DEBUG
	if (!m_method_result.size())
	{
		m_method_result.push_back({1, 0, 3000, 2000, 12125, 15600, 5000, {0, 1, 0}, 0});
		m_method_result.push_back({2, 0, 4000, 3000, 15000, 11600, 7000, {0, 1, 1}, 0});
	}
#endif
	if (m_method_result.size())
	{
		int user_id = m_rep_db.get_record_id(ed_report_maker->model(), ed_report_maker->currentIndex());
		int serial_id  = m_rep_db.get_record_id(ed_akb_number->model(), ed_akb_number->currentIndex());
		const QList<zrm::point_t>* points = m_source->points_get(m_channel);
		QDateTime dtStart;
		uint16_t timeResolution = 0;
		zrm::param_variant pv = param_get(zrm::PARAM_LOG_START);
		if (pv.is_valid())
		{
			zrm::sync_time_t time;
			memcpy(&time, pv.puchar, sizeof (time));
			dtStart.setDate(QDate(time.Year, time.Month, time.Day));
			dtStart.setTime(QTime(time.Hour, time.Min, time.Sec));
			timeResolution = time.mSec;
		}
		bool res = m_rep_db.report_write(user_id, serial_id, m_method_result, dtStat, dtEnd, methodName, minC,
										 mapUBeg, mapUEnd, mapUMax, mapUCrit, mapC, mapTime, rowDetailsControl,
										 dtStart, timeResolution, points);
		QMessageBox::information(this, "Сохранение отчета в БД", (res ? "Отчет успешно сохранен" : "Не удалось сохранить"));
	}
}

void ZrmReport::onState(uint32_t state)
{
	zrm::oper_state_t changes ;
	changes.state = currentState.state ^ state;

	currentState.state = state;
	if (!changes.state)
		return;

	if (currentState.is_executing())
	{
		result_text->clear();
		result_text->setText("Отчет во время выполнения метода недоступен");
		requestTimer.stop();
		return;
	}

	if (changes.is_executing() && !currentState.is_paused() && currentState.is_stopped())
	{
		//qDebug() << Q_FUNC_INFO << " stopped!!!";
		requestTimer.start();
	}
}

