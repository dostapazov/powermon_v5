#ifndef ZRMLOGERCHART_H
#define ZRMLOGERCHART_H

#include <zrmbasewidget.h>
#include "ui_ZrmLogerChart.h"

#include <QDateTime>

namespace QtCharts {
class QLineSeries;
class QXYSeries;
class QDateTimeAxis;
class QValueAxis;
}

class ZrmLogerChart : public ZrmChannelWidget, private Ui::ZrmLogerChart
{
	Q_OBJECT

public:
    explicit ZrmLogerChart(QWidget* parent = nullptr);

protected:
	virtual void on_connected(bool con_state) override;
	virtual void update_controls() override;
    virtual void clear_controls() override;
	virtual void channel_param_changed(unsigned channel, const zrm::params_list_t& params_list) override;
	virtual void channel_session(unsigned channel) override;

    void init_chart();

private slots:
    void setLogStart(const zrm::param_variant& pv);
    void addPoint(const zrm::param_variant& pv);
    void logCount(const zrm::param_variant& pv);

private:
    QtCharts::QChart* m_chart = nullptr;
    QtCharts::QDateTimeAxis* axisTime;
    QtCharts::QLineSeries* u_series = nullptr;
    QtCharts::QLineSeries* i_series = nullptr;
    QMap<int, QtCharts::QXYSeries*> mapSeries;
    QMap<int, QtCharts::QValueAxis*> mapAxis;
    QMap<int, double> mapMin;
    QMap<int, double> mapMax;
    double lastI, lastU;

    QDateTime dtStart;
    uint16_t timeResolution = 0;
    uint count = 0;
    uint32_t timeLast = 0;
};

#endif // ZRMLOGERCHART_H
