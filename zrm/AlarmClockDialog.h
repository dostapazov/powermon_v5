#ifndef ALARMCLOCKDIALOG_H
#define ALARMCLOCKDIALOG_H

#include "ui_AlarmClockDialog.h"

class AlarmClockDialog : public QDialog, private Ui::AlarmClockDialog
{
    Q_OBJECT

public:
    explicit AlarmClockDialog(QWidget *parent = nullptr);

    QByteArray getAlarmClock();

public slots:
    void setAlarmClock(uint size, uint8_t *ba);
};

#endif // ALARMCLOCKDIALOG_H
