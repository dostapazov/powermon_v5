#ifndef ZRMREPORT_H
#define ZRMREPORT_H

#include "ui_zrmreport.h"
#include <zrmbasewidget.h>

#include <zrmreportdatabase.h>
#include <QDateTime>

class ZrmLogerReportChart;

class ZrmReport : public ZrmChannelWidget, private Ui::ZrmReport
{
    Q_OBJECT

    friend class ZrmReportDialog;

public:
    explicit ZrmReport(QWidget* parent = nullptr);

    void setChart(ZrmLogerReportChart* c) { logerChart = c; }

protected slots:
    void save_report();
    void save_report_sql();
    void gen_result_report();
    void akb_type_changed (int idx);
    void getDetails();

protected:
    void save_report_html(const QString& file_name);
    void save_report_pdf(const QString& file_name);
    void save_report_xlsx(const QString& file_name);
    void update_controls() override;
    void clear_controls () override;
    void channel_param_changed(unsigned channel, const zrm::params_list_t& params_list  ) override;
    void channel_session      (unsigned ch_num) override;
    void onActivate() override;
    QString make_report(const QString& a_maker_name, const QString& a_akb_type, const QString& a_akb_number, const zrm::method_exec_results_t& results);

private:
    void onState(uint32_t state);
    zrm::oper_state_t currentState;

    // данные отчета
    zrm::method_exec_results_t m_method_result;
    QMap<uint8_t, int16_t> mapUBeg;
    QMap<uint8_t, int16_t> mapUEnd;
    QMap<uint8_t, int16_t> mapUMax;
    QMap<uint8_t, int16_t> mapUCrit;
    QMap<uint8_t, int32_t> mapC;
    QMap<uint8_t, uint16_t> mapTime;
    int rowDetailsControl = 0;
    QDateTime dtStat, dtEnd;
    QString methodName;
    double minC;
    QString maker_name;
    QString akb_type;
    QString akb_number;
    zrm::method_exec_results_t results_last;

    ZrmReportDatabase m_rep_db;
    QTimer timerDetails;

    QTimer requestTimer;

    ZrmLogerReportChart* logerChart = nullptr;
};

#endif // ZRMREPORT_H
