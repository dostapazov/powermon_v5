#include "AlarmClockDialog.h"

#include "zrmproto.hpp"

AlarmClockDialog::AlarmClockDialog(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
}

QByteArray AlarmClockDialog::getAlarmClock()
{
    QByteArray ba;
    zrm::cdu_time_t time_struct;
    uint size = sizeof (time_struct);
    int baSize = 0;

    auto addClock = [&ba, &baSize, size, &time_struct](uint16_t h, uint8_t m, uint8_t s)
    {
        if (h != 0 || m != 0 || s != 0)
        {
            baSize += size;
            ba.resize(baSize);
            time_struct.h = h;
            time_struct.m = m;
            time_struct.s = s;
            memcpy(ba.data() + baSize - size, &time_struct, size);
        }
    };

    addClock(spinBoxHours1->value(), spinBoxMin1->value(), spinBoxSec1->value());
    addClock(spinBoxHours2->value(), spinBoxMin2->value(), spinBoxSec2->value());
    addClock(spinBoxHours3->value(), spinBoxMin3->value(), spinBoxSec3->value());
    addClock(spinBoxHours4->value(), spinBoxMin4->value(), spinBoxSec4->value());
    addClock(spinBoxHours5->value(), spinBoxMin5->value(), spinBoxSec5->value());

    if (0 == baSize)
    {
        ba.resize(size);
        time_struct.h = 0;
        time_struct.m = 0;
        time_struct.s = 0;
        memcpy(ba.data(), &time_struct, size);
    }

    return ba;
}

void AlarmClockDialog::setAlarmClock(uint size, uint8_t *ba)
{
    zrm::cdu_time_t time_struct;
    uint sizeTime = sizeof (time_struct);
    int count = size / sizeTime;
    if (count > 5)
        count = 5;

    auto setTime = [ba, &time_struct, sizeTime, count](int offset, QSpinBox* sbH, QSpinBox* sbM, QSpinBox* sbS)
    {
        if ((offset + 1) <= count)
        {
            memcpy(&time_struct, ba + offset * sizeTime, sizeTime);
            sbH->setValue(time_struct.h);
            sbM->setValue(time_struct.m);
            sbS->setValue(time_struct.s);
        }
    };

    setTime(0, spinBoxHours1, spinBoxMin1, spinBoxSec1);
    setTime(1, spinBoxHours2, spinBoxMin2, spinBoxSec2);
    setTime(2, spinBoxHours3, spinBoxMin3, spinBoxSec3);
    setTime(3, spinBoxHours4, spinBoxMin4, spinBoxSec4);
    setTime(4, spinBoxHours5, spinBoxMin5, spinBoxSec5);
}
