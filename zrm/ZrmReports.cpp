#include "ZrmReports.h"

ZrmReports::ZrmReports(QWidget* parent) :
    ZrmGroupWidget(parent)
{
    setupUi(this);
    tabReport->setChart(tabChart);
    connect(tabWidget, &QTabWidget::currentChanged, this, &ZrmReports::tabChanged);
}

void ZrmReports::tabChanged(int index)
{
    // обновляем список отчетов
    if (1 == index)
        tabReportBase->read_reports();
}

void ZrmReports::setAdmin(bool a)
{
    tabReportBase->setAdmin(a);
}

void ZrmReports::refreshAKB()
{
    tabReportBase->refreshAKB();
}

void ZrmReports::saveAKB()
{
    tabReportBase->saveAKB();
}
