#include "ZrmLogerChart.h"

#include <zrmparamcvt.h>

#include <signal_bloker.hpp>

#include <QLineSeries>
#include <QScatterSeries>
#include <QDateTimeAxis>
#include <QValueAxis>

constexpr qint64 TIME_LENGTH = 1000 * 60;

ZrmLogerChart::ZrmLogerChart(QWidget* parent) :
    ZrmChannelWidget(parent)
{
    setupUi(this);

    init_chart();
}

void ZrmLogerChart::on_connected(bool con_state)
{
    Q_UNUSED(con_state);
}

void ZrmLogerChart::update_controls()
{
    clear_controls();

    if (m_source && m_channel )
        channel_param_changed(m_channel, m_source->channel_params(m_channel));
}

void ZrmLogerChart::clear_controls()
{
    for (int i = 1; i <= 5; i++)
    {
        mapSeries[i]->clear();
        mapMin[i] = 1000000.;
        mapMax[i] = -1000000.;
    }
    dtStart = QDateTime();
    timeResolution = 0;
    count = 0;
    timeLast = 0;
    labelPointsCount->setText(QString::number(count));
}

void  ZrmLogerChart::channel_param_changed(unsigned channel, const zrm::params_list_t& params_list  )
{
    SignalBlocker sb(findChildren<QWidget*>());
    if (channel == m_channel && m_source)
    {
        for (auto param : params_list)
        {
            switch (param.first)
            {
                case zrm::PARAM_LOG_START :
                    setLogStart(param.second);
                    break;
                case zrm::PARAM_LOG_POINT :
                    addPoint(param.second);
                    break;
                case zrm::PARAM_LOG_COUNT :
                    logCount(param.second);
                    break;
                case zrm::PARAM_MCUR :
                case zrm::PARAM_MCURD :
                {
                    double minValue = ZrmParamCvt::toDouble(param_get(zrm::PARAM_MCURD)).toDouble();
                    double maxValue = ZrmParamCvt::toDouble(param_get(zrm::PARAM_MCUR)).toDouble();
                    mapAxis[1]->setRange(-minValue, maxValue);
                }
                break;
                case zrm::PARAM_MVOLT :
                    mapAxis[2]->setRange(0, ZrmParamCvt::toDouble(param.second).toDouble());
                    break;
                default:
                    break;
            }
        }
    }
    ZrmChannelWidget::channel_param_changed(channel, params_list);
}

void ZrmLogerChart::channel_session(unsigned channel)
{
    if (m_source && m_channel == channel && m_source->channel_session(m_channel).is_active())
    {
        m_source->channel_query_param(m_channel, zrm::PARAM_LOG_START);
        zrm::params_t params;
        params.push_back(zrm::PARAM_LOG_POINT);
        params.push_back(zrm::PARAM_LOG_COUNT);
        m_source->channel_subscribe_params(m_channel, params, true);
    }
}

void ZrmLogerChart::init_chart()
{
    m_chart = new QtCharts::QChart();
    m_chart->setFont(font());
    chart_view->setChart(m_chart);

    chart_view->setRenderHint(QPainter::Antialiasing);

    // ток
    i_series = new QtCharts::QLineSeries(m_chart);
    i_series->setColor(Qt::red);
    i_series->setName(tr("Ток"));
    QPen penI = i_series->pen();
    penI.setWidth(5);
    i_series->setPen(penI);
    m_chart->addSeries(i_series);
    mapSeries[1] = i_series;

    // напряжение
    u_series = new QtCharts::QLineSeries(m_chart);
    u_series->setColor(Qt::darkGreen);
    u_series->setName(tr("Напряжение"));
    QPen penU = u_series->pen();
    penU.setWidth(5);
    u_series->setPen(penU);
    m_chart->addSeries(u_series);
    mapSeries[2] = u_series;

    // ось времени
    QtCharts::QDateTimeAxis* axisX = new QtCharts::QDateTimeAxis;

    axisX->setTickCount(10);
    axisX->setFormat("hh:mm:ss");
    axisX->setTitleText("Time");
    m_chart->addAxis(axisX, Qt::AlignBottom);
    axisTime = axisX;

    QtCharts::QValueAxis* axisYI = new QtCharts::QValueAxis;
    axisYI->setLabelFormat("%.1f");
    axisYI->setTitleText("I");
    m_chart->addAxis(axisYI, Qt::AlignLeft);
    mapAxis[1] = axisYI;
    i_series->attachAxis(axisYI);
    i_series->attachAxis(axisX);

    QtCharts::QValueAxis* axisYU = new QtCharts::QValueAxis;
    axisYU->setLabelFormat("%.1f");
    axisYU->setTitleText("U");
    m_chart->addAxis(axisYU, Qt::AlignLeft);
    mapAxis[2] = axisYU;
    u_series->attachAxis(axisYU);
    u_series->attachAxis(axisX);

    // ошибки
    QtCharts::QScatterSeries* error_series = new QtCharts::QScatterSeries(m_chart);
    error_series->setColor(Qt::darkRed);
    error_series->setMarkerShape(QtCharts::QScatterSeries::MarkerShapeCircle);
    error_series->setMarkerSize(30.0);
    error_series->setName(tr("Код ошибки"));
    m_chart->addSeries(error_series);
    mapSeries[3] = error_series;

    QtCharts::QValueAxis* axisError = new QtCharts::QValueAxis;
    axisError->setTitleText("Err");
    m_chart->addAxis(axisError, Qt::AlignLeft);
    mapAxis[3] = axisError;
    error_series->attachAxis(axisError);
    error_series->attachAxis(axisX);
    axisError->setVisible(false);

    // операция
    QtCharts::QScatterSeries* operation_series = new QtCharts::QScatterSeries(m_chart);
    operation_series->setColor(Qt::black);
    operation_series->setMarkerShape(QtCharts::QScatterSeries::MarkerShapeCircle);
    operation_series->setMarkerSize(15.0);
    operation_series->setName(tr("Операция"));
    m_chart->addSeries(operation_series);
    mapSeries[4] = operation_series;

    QtCharts::QValueAxis* axisOperation = new QtCharts::QValueAxis;
    axisOperation->setTitleText("Op");
    m_chart->addAxis(axisOperation, Qt::AlignLeft);
    mapAxis[4] = axisOperation;
    operation_series->attachAxis(axisOperation);
    operation_series->attachAxis(axisX);
    axisOperation->setVisible(false);

    // этап
    QtCharts::QScatterSeries* stage_series = new QtCharts::QScatterSeries(m_chart);
    stage_series->setColor(Qt::magenta);
    stage_series->setMarkerShape(QtCharts::QScatterSeries::MarkerShapeRectangle);
    stage_series->setMarkerSize(15.0);
    stage_series->setName(tr("Этап"));
    m_chart->addSeries(stage_series);
    mapSeries[5] = stage_series;

    QtCharts::QValueAxis* axisStage = new QtCharts::QValueAxis;
    axisStage->setTitleText("Stage");
    m_chart->addAxis(axisStage, Qt::AlignLeft);
    mapAxis[5] = axisStage;
    stage_series->attachAxis(axisStage);
    stage_series->attachAxis(axisX);
    axisStage->setVisible(false);
}

void ZrmLogerChart::setLogStart(const zrm::param_variant& pv)
{
    clear_controls();

    if (pv.is_valid())
    {
        /*QByteArray ba;
        ba.resize(pv.size);
        memcpy(ba.data(), pv.pchar, pv.size);
        qDebug() << "start" << ba.toHex();*/
        zrm::sync_time_t time;
        memcpy(&time, pv.puchar, sizeof (time));
        dtStart.setDate(QDate(time.Year, time.Month, time.Day));
        dtStart.setTime(QTime(time.Hour, time.Min, time.Sec));
        timeResolution = time.mSec;
        //qDebug() << "start" << timeResolution << dtStart;

        axisTime->setMin(dtStart);
        axisTime->setMax(dtStart.addSecs(60));
    }
}

void ZrmLogerChart::addPoint(const zrm::param_variant& pv)
{
    if (pv.is_valid())
    {
        uint packLength = pv.size;
        // число точек
        int pointCount = packLength / 9;
        zrm::point_t point;
        int pointSize = sizeof (zrm::point_t);
        for (int i = 0; i < pointCount; i++)
        {
            count++;
            labelPointsCount->setText(QString::number(count));
            memcpy(&point, pv.puchar + i * pointSize, pointSize);
            //qDebug() << "point" << count <<  point.id << point.time << point.value;
            if (point.id > 5)
                continue;

            // время точки
            qint64 ms = dtStart.toMSecsSinceEpoch() + point.time * timeResolution;
            // значение точки
            double v = double(point.value);
            if (point.id <= 2)
                v = double(point.value) / 1000.;

            // расчет диапазона графика
            if (mapMin[point.id] > v)
                mapMin[point.id] = v;
            if (mapMax[point.id] < v)
                mapMax[point.id] = v;
            if (mapMin[i] == mapMax[i])
            {
                if (mapMin[i] >= 1)
                    mapMin[i] -= 1;
                else
                    mapMax[i] += 1;
            }

            // добавляем точку
            if (point.time >= timeLast)
            {
                mapSeries[point.id]->append(ms, v);
                timeLast = point.time;
            }
            else if (point.time * timeResolution >= (timeLast * timeResolution - TIME_LENGTH))
            {
                // вставляем не в конец
                for (int i = mapSeries[point.id]->count() - 1; i >= 0; i--)
                    if (mapSeries[point.id]->at(i).x() >= point.time)
                    {
                        mapSeries[point.id]->insert(i + 1, QPointF(ms, v));
                        break;
                    }
            }

            // диапазон графика
            if (point.id > 2)
                mapAxis[point.id]->setRange(mapMin[point.id], mapMax[point.id]);

            // запоминаем последний параметр для повторной отрисовки
            if (1 == point.id)
                lastI = v;
            if (2 == point.id)
                lastU = v;

            // если параметр не пришел, то он не изменился
            if (1 != point.id && i_series->count() > 0 && ms!= i_series->points().last().x())
                i_series->append(ms, lastI);
            if (2 != point.id && u_series->count() > 0 && ms!= u_series->points().last().x())
                u_series->append(ms, lastU);
        }

        // удаляем старые точки
        for (int i = 1; i <= 5; i++)
        {
            // вычисляем количество кочек
            int count = 0;
            qint64 mslast = 0;
            for (count = 0; count < mapSeries[i]->count(); count++)
            {
                mslast = dtStart.toMSecsSinceEpoch() + timeLast * timeResolution - TIME_LENGTH;
                if (mapSeries[i]->at(count).x() >= mslast)
                    break;
            }
            if (0 == count)
                continue;
            if (i > 2)
                // точки на графике просто зачищаем
                mapSeries[i]->removePoints(0, count);
            else
            {
                // линии тока и напряжения должны быть непрерывны
                if (mapSeries[i]->at(count).x() == mslast)
                    mapSeries[i]->removePoints(0, count);
                else
                {
                    // если не попали по времени ровно сдвигаем точку
                    mapSeries[i]->replace(count - 1, mslast, mapSeries[i]->at(count).y());
                    if (count > 2)
                        mapSeries[i]->removePoints(0, count - 1);
                }
            }
        }

        qint64 ms = dtStart.toMSecsSinceEpoch() + timeLast * timeResolution;
        QDateTime endRange = QDateTime::fromMSecsSinceEpoch(ms);
        QDateTime begRange = endRange.addMSecs(-TIME_LENGTH);
        axisTime->setRange(begRange, endRange);
    }
}

void ZrmLogerChart::logCount(const zrm::param_variant& pv)
{
    // pv нельзя использовать
    // так как порядок обработки значений может отличаться от порядка их передачи
    Q_UNUSED(pv)

    zrm::param_variant p = param_get(zrm::PARAM_LOG_COUNT);
    if (p.is_valid())
    {
        if (p.size >= 4)
        {
            uint32_t lastPoint;
            memcpy(&lastPoint, p.pchar, 4);
            labelPointsLast->setText(QString::number(lastPoint));

            if (p.size >= 8)
            {
                uint32_t totalPoint;
                memcpy(&totalPoint, p.pchar + 4, 4);
                labelPointsTotal->setText(QString::number(totalPoint));
                //qDebug() << "log count" << lastPoint << totalPoint;

                // перезапрос графика
                if (p.size >= 9)
                {
                    uint8_t isnew;
                    memcpy(&isnew, p.pchar + 8, 1);
                    if (1 == isnew)
                        m_source->channel_query_param(m_channel, zrm::PARAM_LOG_START);
                }
            }
        }

    }
}
