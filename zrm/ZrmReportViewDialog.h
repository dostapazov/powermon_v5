﻿#ifndef ZRMREPORTDIALOG_H
#define ZRMREPORTDIALOG_H

#include "ui_ZrmReportViewDialog.h"

#include <QDateTime>

namespace QtCharts
{
    class QLineSeries;
    class QXYSeries;
    class QDateTimeAxis;
    class QValueAxis;
}

class ZrmReportViewDialog : public QDialog, private Ui::ZrmReportViewDialog
{
    Q_OBJECT

public:
    explicit ZrmReportViewDialog(QWidget *parent = nullptr);

    void setReportId(qlonglong id);
    void setResultText(QString result);

protected slots:
    void save_report();
    void openReportFromBase();

protected:
    void save_report_html(const QString & file_name);
    void save_report_pdf (const QString & file_name);
    void save_report_xlsx(const QString & file_name);

    // chart
    void init_chart();
    void showChart();

private slots:
    void loadPoints();
    void resetTime();
    void scrollLeft();
    void scrollRight();
    void zoomIn();
    void zoomOut();

private:
    qlonglong idReport = -1;

    QtCharts::QChart * chart = nullptr;
    QMap<int, QtCharts::QXYSeries*> mapSeries;
    QtCharts::QDateTimeAxis *axisTime;
    QtCharts::QLineSeries* u_series = nullptr;
    QtCharts::QLineSeries* i_series = nullptr;
    QMap<int, QtCharts::QValueAxis*> mapAxis;
    QMap<int, double> mapMin;
    QMap<int, double> mapMax;
    double lastI, lastU;
    QDateTime dtStart;
    uint16_t timeResolution = 0;
    void initSaveButtons();
};

#endif // ZRMREPORTDIALOG_H
