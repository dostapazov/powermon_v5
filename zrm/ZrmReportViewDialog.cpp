﻿#include "ZrmReportViewDialog.h"

#include "zrmreportdatabase.h"
#include "xlsxdocument.h"

#include <QSqlRecord>
#include <QSqlQuery>
#include <QFileDialog>
#include <QStandardPaths>
#include <QProcess>
#include <QPrinter>
#include <QTimer>

#include <QLineSeries>
#include <QScatterSeries>
#include <QValueAxis>
#include <QDateTimeAxis>

#include <QDesktopServices>

ZrmReportViewDialog::ZrmReportViewDialog(QWidget* parent) :
	QDialog(parent)
{
	setupUi(this);
	init_chart();
	initSaveButtons();

	init_chart();
	frameButtons->setEnabled(false);
	//setWindowFlags(Qt::Window);
	resize(800, 600);
	showMaximized();

	//cb_report_details->setChecked(true);

	connect(tbSaveHtml, &QAbstractButton::clicked, this, &ZrmReportViewDialog::save_report);
	connect(tbSavePdf, &QAbstractButton::clicked, this, &ZrmReportViewDialog::save_report);
	connect(tbSaveXlsx, &QAbstractButton::clicked, this, &ZrmReportViewDialog::save_report);
	connect(cb_report_details, &QAbstractButton::clicked, this,
	[this]() { chartView->setVisible(cb_report_details->isChecked()); frameButtons->setVisible(cb_report_details->isChecked()); } );

	connect(pushButtonReset, SIGNAL(clicked(bool)), this, SLOT(resetTime()));
	connect(pushButtonScrollLeft, SIGNAL(clicked(bool)), this, SLOT(scrollLeft()));
	connect(pushButtonScrollRight, SIGNAL(clicked(bool)), this, SLOT(scrollRight()));
	connect(pushButtonIn, SIGNAL(clicked(bool)), this, SLOT(zoomIn()));
	connect(pushButtonOut, SIGNAL(clicked(bool)), this, SLOT(zoomOut()));
}

void ZrmReportViewDialog::setReportId(qlonglong id)
{
	idReport = id;
	cb_report_details->setEnabled(true);
	cb_report_details->setVisible(true);
	openReportFromBase();

	QTimer::singleShot(1000, this, SLOT(loadPoints()) );
}

void ZrmReportViewDialog::setResultText(QString result)
{
	cb_report_details->setEnabled(false);
	cb_report_details->setVisible(false);
	result_text->setText(result);
	result_text->moveCursor(QTextCursor::MoveOperation::Start);
}

void ZrmReportViewDialog::save_report_html(const QString& file_name)
{
	QFile file(file_name);
	if (result_text && file.open(QFile::WriteOnly | QFile::Truncate))
		file.write(result_text->toHtml().toUtf8());
}

void ZrmReportViewDialog::save_report_pdf (const QString& file_name)
{
	QPrinter printer(QPrinter::ScreenResolution);
	printer.setOutputFormat(QPrinter::PdfFormat);
#if QT_VERSION_CHECK(5,15,0) > QT_VERSION
	printer.setOrientation(cb_report_details->isChecked() ? QPrinter::Orientation::Landscape : QPrinter::Orientation::Portrait);
#else
	printer.setPageOrientation(cb_report_details->isChecked() ? QPageLayout::Landscape : QPageLayout::Portrait);
#endif
	printer.setOutputFileName(file_name);
	//result_text->print(&printer);

	QTextEdit te(result_text->toHtml());
	if (cb_report_details->isChecked())
	{
		// отрисовка графика
		QTextDocument* doc = te.document();
		int mw = chartView->maximumWidth();
		chartView->setMaximumWidth(printer.width() - 50);
		resetTime();
		QPixmap pix = chartView->grab();
		chartView->setMaximumWidth(mw);
		QImage image(pix.toImage());
		doc->addResource(QTextDocument::ImageResource, QUrl("image"), image);
		QTextCursor cursor = te.textCursor();
		cursor.movePosition(QTextCursor::End);
		cursor.insertImage("image");
	}
	te.print(&printer);
}

void ZrmReportViewDialog::save_report_xlsx(const QString& file_name)
{
	QXlsx::Document xlsx;
	int rowNum = 1;

	QFont f = result_text->font();
	f.setBold(true);
	QXlsx::Format headerFmt;
	headerFmt.setFont(f);
	QXlsx::Format tableFmt;
	tableFmt.setFont(f);
	tableFmt.setBorderStyle(QXlsx::Format::BorderMedium);
	QFontMetrics fm(f);
	int chWidth =  fm.horizontalAdvance('Q') / 2;

	ZrmReportDatabase rep_database;
	QString qtext =
		"SELECT r.id, bt.id AS id_akb_type, bt.name AS akb_type, bl.id AS id_akb_number, bl.serial_number AS akb_number, r.id_user, u.short_fio, "
		"CAST(r.dtm AS text) dtm, r.total_duration, r.total_energy, r.total_capacity, r.dt_start, r.dt_end, r.method_name, r.c_min "
		"FROM treport r "
		"LEFT JOIN tusers u ON u.id = r.id_user "
		"LEFT JOIN tbattery_list bl ON bl.id = r.id_battery "
		"LEFT JOIN tbattery_types bt ON bt.id = bl.id_type "
		"WHERE r.id = :id ; ";
	QSqlQuery query(*rep_database.database());
	if (!query.prepare(qtext))
		return;

	query.bindValue(":id", idReport);

	if (!query.exec() || !query.next())
		return;

	QSqlRecord rec = query.record();
	if (rec.isEmpty())
		return;

	xlsx.write(rowNum++, 1, tr("Отчет об обслуживании АКБ"), headerFmt);
	rowNum++;

	xlsx.write(rowNum++, 1, tr("Тип АКБ    %1 № %2      Ответственный %3").arg(rec.value("akb_type").toString(), rec.value("akb_number").toString(), rec.value("short_fio").toString()), headerFmt);
	xlsx.write(rowNum++, 1, tr("Дата начало: %1; завершение: %2")
			   .arg(rec.value("dt_start").toDateTime().toString("yyyy-MM-dd hh:mm:ss"))
			   .arg(rec.value("dt_end").toDateTime().toString("yyyy-MM-dd hh:mm:ss")), headerFmt);
	auto hms = pwm_utils::secunds2hms(uint32_t(rec.value("total_duration").toInt()));
	xlsx.write(rowNum++, 1, tr("Время выполнения %1:%2:%3")
			   .arg(std::get<0>(hms), 2, 10, QChar('0'))
			   .arg(std::get<1>(hms), 2, 10, QChar('0'))
			   .arg(std::get<2>(hms), 2, 10, QChar('0')), headerFmt);
	xlsx.write(rowNum++, 1, tr("Профиль (метод): %1").arg(rec.value("method_name").toString()), headerFmt);
	double total_energy = rec.value("total_energy").toDouble();
	xlsx.write(rowNum++, 1, tr("%1 %2 А*Ч").arg(total_energy < 0 ? tr("Из АКБ потреблено") : tr("В АКБ передано")).arg(fabs(total_energy), 0, 'f', 2), headerFmt);
	double total_capacity = rec.value("total_capacity").toDouble();
	double min_capacity = rec.value("min_c").toDouble();
	if (!qFuzzyIsNull(total_capacity))
		xlsx.write(rowNum++, 1, tr("Ёмкость АКБ %1 А*Ч      Минимальная ёмкость АКБ %2 А").arg(total_capacity, 0, 'f', 2).arg(min_capacity, 0, 'f', 2), headerFmt);

	double Ubeg = 0., Uend = 0., Iend = 0.;
	QString qtextBeg =
		"SELECT u_beg FROM treport_detaits WHERE id_report = :id "
		"AND stage_number = (SELECT MIN(stage_number) FROM treport_details WHERE id_report = :id) ; ";
	QSqlQuery queryBeg(*rep_database.database());
	if (queryBeg.prepare(qtextBeg))
	{
		queryBeg.bindValue(":id", idReport);
		queryBeg.exec();
		queryBeg.next();
		QSqlRecord recBeg = queryBeg.record();
		if (!recBeg.isEmpty())
			Ubeg = recBeg.value(0).toDouble();
	}
	QString qtextEnd =
		"SELECT u_end, i_end FROM treport_detaits WHERE id_report = :id "
		"AND stage_number = (SELECT MAX(stage_number) FROM treport_details WHERE id_report = :id) ; ";
	QSqlQuery queryEnd(*rep_database.database());
	if (queryEnd.prepare(qtextEnd))
	{
		queryEnd.bindValue(":id", idReport);
		queryEnd.exec();
		queryEnd.next();
		QSqlRecord recEnd = queryEnd.record();
		if (!recEnd.isEmpty())
		{
			Uend = recEnd.value(0).toDouble();
			Iend = recEnd.value(1).toDouble();
		}
	}
	xlsx.write(rowNum++, 1, tr("Напряжение начальное %1 В; конечное %2 В; ток %3 А").arg(Ubeg, 0, 'f', 2).arg(Uend, 0, 'f', 2).arg(Iend, 0, 'f', 2), headerFmt);
	rowNum++;

	xlsx.write(rowNum++, 1, tr("Результаты этапов"), headerFmt);
	int col = 1;
	xlsx.write(rowNum, col++, tr("Этап"), tableFmt);
	//xlsx.setColumnWidth(i + 1, tableTime->columns.at(i).defWidth / chWidth);
	xlsx.write(rowNum, col++, tr("I нач"), tableFmt);
	xlsx.write(rowNum, col++, tr("I кон"), tableFmt);
	xlsx.write(rowNum, col++, tr("U нач"), tableFmt);
	xlsx.write(rowNum, col++, tr("U кон"), tableFmt);
	QString capacity = tr("Ёмкость");
	xlsx.write(rowNum, col, capacity, tableFmt);
	xlsx.setColumnWidth(col++, fm.horizontalAdvance(capacity) / chWidth);
	QString duration = tr("Время, мин");
	xlsx.write(rowNum, col, duration, tableFmt);
	xlsx.setColumnWidth(col++, fm.horizontalAdvance(duration) / chWidth);
	rowNum++;

	QString qtextdetail =
		"SELECT stage_number, stage_duration, u_beg, i_beg, u_end, i_end, capacity "
		"FROM treport_details "
		"WHERE id_report = :id "
		"ORDER BY stage_number ";
	QSqlQuery querydetail(*rep_database.database());
	if (!querydetail.prepare(qtextdetail))
		return;
	querydetail.bindValue(":id", idReport);
	querydetail.exec();

	while (querydetail.next())
	{
		QSqlRecord recdetail = querydetail.record();
		double CAP  = recdetail.value("capacity").toDouble();
		double Ibeg = recdetail.value("i_beg").toDouble();
		double Iend = recdetail.value("i_end").toDouble();
		double Ubeg = recdetail.value("u_beg").toDouble();
		double Uend = recdetail.value("u_end").toDouble();
		int duration = recdetail.value("stage_duration").toInt();

		col = 1;
		xlsx.write(rowNum, col++, recdetail.value("stage_number").toInt(), tableFmt);
		xlsx.write(rowNum, col++, QString::number(Ibeg, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString::number(Iend, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString::number(Ubeg, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString::number(Uend, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString::number(CAP, 'f', 2), tableFmt);
		xlsx.write(rowNum, col++, QString("%1:%2:%3").arg(duration / 3600, 2, 10, QLatin1Char('0'))
				   .arg((duration - duration / 3600) / 60, 2, 10, QLatin1Char('0'))
				   .arg(duration % 60, 2, 10, QLatin1Char('0')), tableFmt);
		rowNum++;
	}
	rowNum++;

	xlsx.write(rowNum++, 1, tr("Отчёт поаккумуляторного контроля"), headerFmt);
	col = 1;
	xlsx.write(rowNum, col++, tr("№"), tableFmt);
	xlsx.write(rowNum, col++, tr("U нач"), tableFmt);
	xlsx.write(rowNum, col++, tr("U кон"), tableFmt);
	xlsx.write(rowNum, col++, tr("U макс"), tableFmt);
	xlsx.write(rowNum, col++, tr("U крит"), tableFmt);
	xlsx.write(rowNum, col++, tr("Ёмкость"), tableFmt);
	xlsx.write(rowNum, col++, tr("Время, мин"), tableFmt);
	rowNum++;

	QString strControl =
		"SELECT number, u_beg, u_end, u_max, u_crit, c, time "
		"FROM treport_details_control "
		"WHERE id_report = :id "
		"ORDER BY number;";
	QSqlQuery queryControl(*rep_database.database());
	if (!queryControl.prepare(strControl))
		return;
	queryControl.bindValue(":id", idReport);

	if (queryControl.exec())
	{
		while (queryControl.next())
		{
			QSqlRecord recControl = queryControl.record();
			col = 1;
			xlsx.write(rowNum, col++, recControl.value("number").toInt(), tableFmt);
			xlsx.write(rowNum, col++, !recControl.isNull("u_beg") ? QString::number(recControl.value("u_beg").toDouble(), 'f', 2) : "", tableFmt);
			xlsx.write(rowNum, col++, !recControl.isNull("u_end") ? QString::number(recControl.value("u_end").toDouble(), 'f', 2) : "", tableFmt);
			xlsx.write(rowNum, col++, !recControl.isNull("u_max") ? QString::number(recControl.value("u_max").toDouble(), 'f', 2) : "", tableFmt);
			xlsx.write(rowNum, col++, !recControl.isNull("u_crit") ? QString::number(recControl.value("u_crit").toDouble(), 'f', 2) : "", tableFmt);
			xlsx.write(rowNum, col++, !recControl.isNull("c") ? QString::number(recControl.value("c").toDouble(), 'f', 2) : "", tableFmt);
			xlsx.write(rowNum, col++, !recControl.isNull("time") ? QString::number(recControl.value("time").toInt()) : "", tableFmt);
			rowNum++;
		}
	}

	xlsx.saveAs(file_name);
}

constexpr const char* const DOC_FILTER = "FileFilter";

void ZrmReportViewDialog::initSaveButtons()
{
	tbSaveHtml->setProperty(DOC_FILTER, "HTML (*.html *.htm)");
	tbSavePdf->setProperty(DOC_FILTER, "PDF (*.pdf)");
	tbSaveXlsx->setProperty(DOC_FILTER, tr("Файл EXCEL (*.xlsx)"));
}

void ZrmReportViewDialog::save_report()
{
#ifdef Q_OS_ANDROID
	QString doc_dir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
#else
	QString doc_dir = qApp->applicationDirPath();
#endif

	QObject* s = sender();
	QString filter = s ? s->property(DOC_FILTER).toString() : QString();
	if (filter.isEmpty())
		return;

	QString file_name = QFileDialog::getSaveFileName(this, tr("Сохранение результатов"), doc_dir, filter);

	if (file_name.isEmpty())
		return;

	if (tbSaveHtml == s)
		save_report_html(file_name);
	if (tbSavePdf == s)
		save_report_pdf(file_name);
	if (tbSaveXlsx == s)
		save_report_xlsx(file_name);
#ifdef Q_OS_WINDOWS
	QProcess* console = new QProcess(this);
	console->start("cmd.exe", QStringList() << "/C" << file_name);
#else
	QUrl url = QUrl::fromLocalFile(file_name);
	QDesktopServices::openUrl(url);
#endif
}

void ZrmReportViewDialog::openReportFromBase()
{
	if (idReport <= 0)
		return;

	ZrmReportDatabase rep_database;
	QString qtext =
		"SELECT r.id, bt.id AS id_akb_type, bt.name AS akb_type, bl.id AS id_akb_number, bl.serial_number AS akb_number, r.id_user, u.short_fio, "
		"CAST(r.dtm AS text) dtm, r.total_duration, r.total_energy, r.total_capacity, r.dt_start, r.dt_end, r.method_name, r.c_min "
		"FROM treport r "
		"LEFT JOIN tusers u ON u.id = r.id_user "
		"LEFT JOIN tbattery_list bl ON bl.id = r.id_battery "
		"LEFT JOIN tbattery_types bt ON bt.id = bl.id_type "
		"WHERE r.id = :id ; ";
	QSqlQuery query(*rep_database.database());
	if (!query.prepare(qtext))
		return;
	query.bindValue(":id", idReport);
	query.exec();
	query.next();
	QSqlRecord rec = query.record();
	if (rec.isEmpty())
		return;

	QString result;
	QStringList main_text ;
	QStringList  details_table;
	QString qtextdetail =
		"SELECT stage_number, stage_duration, u_beg, i_beg, u_end, i_end, capacity "
		"FROM treport_details "
		"WHERE id_report = :id "
		"ORDER BY stage_number ";
	QSqlQuery querydetail(*rep_database.database());
	if (!querydetail.prepare(qtextdetail))
		return;
	querydetail.bindValue(":id", idReport);
	querydetail.exec();

	details_table += QString("<table width=600 border=1><caption><h3>%1</h3></caption> ").arg(tr("Результаты этапов"));
	details_table +=
		QString("<tr align=center><td>%1</td> <td>%2</td> <td>%3</td> <td>%4</td> <td>%5</td> <td>%6</td> <td>%7</td></tr> ")
		.arg(tr("Этап" ), tr("I нач"), tr("I кон"), tr("U нач"), tr("U кон"), tr("Ёмкость"), tr("Время"));

	QString detail_row = tr("<tr align=center><td>%1</td> "
							"<td style=\"text-align: right;\">%2 A</td> "
							"<td style=\"text-align: right;\">%3 А</td> "
							"<td style=\"text-align: right;\">%4 В</td> "
							"<td style=\"text-align: right;\">%5 В</td> "
							"<td style=\"text-align: right;\">%6 А*Ч</td>"
							"<td>%7</td></tr>");
	while (querydetail.next())
	{
		QSqlRecord recdetail = querydetail.record();
		double CAP  = recdetail.value("capacity").toDouble();
		double Ibeg = recdetail.value("i_beg").toDouble();
		double Iend = recdetail.value("i_end").toDouble();
		double Ubeg = recdetail.value("u_beg").toDouble();
		double Uend = recdetail.value("u_end").toDouble();
		int duration = recdetail.value("stage_duration").toInt();

		details_table += detail_row.arg(recdetail.value("stage_number").toInt())
						 .arg(Ibeg, 0, 'f', 2)
						 .arg(Iend, 0, 'f', 2)
						 .arg(Ubeg, 0, 'f', 2)
						 .arg(Uend, 0, 'f', 2)
						 .arg(CAP, 0, 'f', 2)
						 .arg(tr("%1:%2:%3").arg(duration / 3600, 2, 10, QLatin1Char('0'))
							  .arg((duration - duration / 3600) / 60, 2, 10, QLatin1Char('0'))
							  .arg(duration % 60, 2, 10, QLatin1Char('0'))
							 );
	}

	details_table += tr("</table>\n");

	// data control
	details_table += QString("<table width=600 border=1><caption><h3>%1</h3></caption> ").arg(tr("Отчёт поаккумуляторного контроля"));
	details_table +=
		QString("<tr align=center><td>%1</td> <td>%2</td> <td>%3</td> <td>%4</td> <td>%5</td> <td>%6</td> <td>%7</td></tr> ")
		.arg(tr("№" ), tr("U нач"), tr("U кон"), tr("U макс"), tr("U крит"), tr("Ёмкость"), tr("Время, мин"));

	QString control_row = tr("<tr align=center><td>%1</td> "
							 "<td style=\"text-align: right;\">%2</td> "
							 "<td style=\"text-align: right;\">%3</td> "
							 "<td style=\"text-align: right;\">%4</td> "
							 "<td style=\"text-align: right;\">%5</td> "
							 "<td style=\"text-align: right;\">%6</td> "
							 "<td>%7</td></tr>");

	QString strControl =
		"SELECT number, u_beg, u_end, u_max, u_crit, c, time "
		"FROM treport_details_control "
		"WHERE id_report = :id "
		"ORDER BY number;";
	QSqlQuery queryControl(*rep_database.database());
	if (!queryControl.prepare(strControl))
		return;
	queryControl.bindValue(":id", idReport);
	queryControl.exec();
	while (queryControl.next())
	{
		QSqlRecord recControl = queryControl.record();
		details_table += control_row
						 .arg(recControl.value("number").toInt())
						 .arg(!recControl.isNull("u_beg") ? QString::number(recControl.value("u_beg").toDouble(), 'f', 2) : "")
						 .arg(!recControl.isNull("u_end") ? QString::number(recControl.value("u_end").toDouble(), 'f', 2) : "")
						 .arg(!recControl.isNull("u_max") ? QString::number(recControl.value("u_max").toDouble(), 'f', 2) : "")
						 .arg(!recControl.isNull("u_crit") ? QString::number(recControl.value("u_crit").toDouble(), 'f', 2) : "")
						 .arg(!recControl.isNull("c") ? QString::number(recControl.value("c").toDouble(), 'f', 2) : "")
						 .arg(!recControl.isNull("time") ? QString::number(recControl.value("time").toInt()) : "");
	}

	details_table += tr("</table>\n");

	main_text += tr("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\" http://www.w3.org\">");
	main_text += tr("<html><head>");
	main_text += tr("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
	main_text += tr("<title>%1</title>").arg(rec.value("dtm").toDateTime().toString("dd-MM-yyyy hh:mm:ss"));
	main_text += tr("<style type=\"text/css\"> td { white-space: nowrap; } </style>");
	main_text += tr("</head><body><header><h2>%1</h2></header>").arg("Отчет об обслуживании АКБ");

	main_text += tr("<p style=\"white-space: pre-wrap;\">Тип АКБ    %1 № %2      Ответственный %3<br>").arg(rec.value("akb_type").toString(), rec.value("akb_number").toString(), rec.value("short_fio").toString());
	main_text += tr("Дата начало: %1; завершение: %2<br>")
				 .arg(rec.value("dt_start").toDateTime().toString("yyyy-MM-dd hh:mm:ss"))
				 .arg(rec.value("dt_start").toDateTime().toString("yyyy-MM-dd hh:mm:ss"));
	auto hms = pwm_utils::secunds2hms(uint32_t(rec.value("total_duration").toInt()));
	main_text += tr("Время выполнения %1:%2:%3<br>")
				 .arg(std::get<0>(hms), 2, 10, QChar('0'))
				 .arg(std::get<1>(hms), 2, 10, QChar('0'))
				 .arg(std::get<2>(hms), 2, 10, QChar('0'));
	main_text += tr("Профиль (метод): %1<br>").arg(rec.value("method_name").toString());

	double total_energy = rec.value("total_energy").toDouble();
	main_text += tr("%1 %2 А*Ч<br>").arg(total_energy < 0 ? tr("Из АКБ потреблено") : tr("В АКБ передано"))
				 .arg(fabs(total_energy), 0, 'f', 2);

	double total_capacity = rec.value("total_capacity").toDouble();
	double min_capacity = rec.value("min_c").toDouble();
	if (!qFuzzyIsNull(total_capacity))
		main_text += tr("Ёмкость АКБ %1 А*Ч      Минимальная ёмкость АКБ %2 А<br>").arg(total_capacity, 0, 'f', 2).arg(min_capacity, 0, 'f', 2);

	double Ubeg = 0., Uend = 0., Iend = 0.;
	QString qtextBeg =
		"SELECT u_beg FROM treport_detaits WHERE id_report = :id "
		"AND stage_number = (SELECT MIN(stage_number) FROM treport_details WHERE id_report = :id) ; ";
	QSqlQuery queryBeg(*rep_database.database());
	if (queryBeg.prepare(qtextBeg))
	{
		queryBeg.bindValue(":id", idReport);
		queryBeg.exec();
		queryBeg.next();
		QSqlRecord recBeg = queryBeg.record();
		if (!recBeg.isEmpty())
			Ubeg = recBeg.value(0).toDouble();
	}
	QString qtextEnd =
		"SELECT u_end, i_end FROM treport_detaits WHERE id_report = :id "
		"AND stage_number = (SELECT MAX(stage_number) FROM treport_details WHERE id_report = :id) ; ";
	QSqlQuery queryEnd(*rep_database.database());
	if (queryEnd.prepare(qtextEnd))
	{
		queryEnd.bindValue(":id", idReport);
		queryEnd.exec();
		queryEnd.next();
		QSqlRecord recEnd = queryEnd.record();
		if (!recEnd.isEmpty())
		{
			Uend = recEnd.value(0).toDouble();
			Iend = recEnd.value(1).toDouble();
		}
	}
	main_text += tr("Напряжение начальное %1 В; конечное %2 В; ток %3 А<br>").arg(Ubeg, 0, 'f', 2).arg(Uend, 0, 'f', 2).arg(Iend, 0, 'f', 2);
	main_text += tr("</p>\n");

	main_text.append(details_table);
	main_text += tr("</body></html>");

	result = main_text.join(QChar('\n'));

	result_text->setText(result);
	result_text->moveCursor(QTextCursor::MoveOperation::Start);
}

// chart

void ZrmReportViewDialog::init_chart()
{
	chart = new QtCharts::QChart();
	chart->setFont(font());
	chartView->setChart(chart);

	chartView->setRenderHint(QPainter::Antialiasing);

	// ток
	i_series = new QtCharts::QLineSeries(chart);
	i_series->setColor(Qt::red);
	i_series->setName(tr("Ток"));
	QPen penI = i_series->pen();
	penI.setWidth(5);
	i_series->setPen(penI);
	mapSeries[1] = i_series;

	// напряжение
	u_series = new QtCharts::QLineSeries(chart);
	u_series->setColor(Qt::darkGreen);
	u_series->setName(tr("Напряжение"));
	QPen penU = u_series->pen();
	penU.setWidth(5);
	u_series->setPen(penU);
	mapSeries[2] = u_series;

	// ось времени
	QtCharts::QDateTimeAxis* axisX = new QtCharts::QDateTimeAxis;

	axisX->setTickCount(10);
	axisX->setFormat("hh:mm:ss");
	axisX->setTitleText("Time");
	chart->addAxis(axisX, Qt::AlignBottom);
	axisTime = axisX;

	QtCharts::QValueAxis* axisYI = new QtCharts::QValueAxis;
	axisYI->setLabelFormat("%.1f");
	axisYI->setTitleText("I");
	chart->addAxis(axisYI, Qt::AlignLeft);
	mapAxis[1] = axisYI;

	QtCharts::QValueAxis* axisYU = new QtCharts::QValueAxis;
	axisYU->setLabelFormat("%.1f");
	axisYU->setTitleText("U");
	chart->addAxis(axisYU, Qt::AlignLeft);
	mapAxis[2] = axisYU;

	// ошибки
	QtCharts::QScatterSeries* error_series = new QtCharts::QScatterSeries(chart);
	error_series->setColor(Qt::darkRed);
	error_series->setMarkerShape(QtCharts::QScatterSeries::MarkerShapeCircle);
	error_series->setMarkerSize(30.0);
	error_series->setName(tr("Код ошибки"));
	mapSeries[3] = error_series;

	QtCharts::QValueAxis* axisError = new QtCharts::QValueAxis;
	axisError->setTitleText("Err");
	chart->addAxis(axisError, Qt::AlignLeft);
	mapAxis[3] = axisError;
	axisError->setVisible(false);

	// операция
	QtCharts::QScatterSeries* operation_series = new QtCharts::QScatterSeries(chart);
	operation_series->setColor(Qt::black);
	operation_series->setMarkerShape(QtCharts::QScatterSeries::MarkerShapeCircle);
	operation_series->setMarkerSize(15.0);
	operation_series->setName(tr("Операция"));
	mapSeries[4] = operation_series;

	QtCharts::QValueAxis* axisOperation = new QtCharts::QValueAxis;
	axisOperation->setTitleText("Op");
	chart->addAxis(axisOperation, Qt::AlignLeft);
	mapAxis[4] = axisOperation;
	axisOperation->setVisible(false);

	// этап
	QtCharts::QScatterSeries* stage_series = new QtCharts::QScatterSeries(chart);
	stage_series->setColor(Qt::magenta);
	stage_series->setMarkerShape(QtCharts::QScatterSeries::MarkerShapeRectangle);
	stage_series->setMarkerSize(15.0);
	stage_series->setName(tr("Этап"));
	mapSeries[5] = stage_series;

	QtCharts::QValueAxis* axisStage = new QtCharts::QValueAxis;
	axisStage->setTitleText("Stage");
	chart->addAxis(axisStage, Qt::AlignLeft);
	mapAxis[5] = axisStage;
	axisStage->setVisible(false);
}

void ZrmReportViewDialog::loadPoints()
{
	ZrmReportDatabase rep_database;

	// дата и множитель времени
	QString qTextStart = "SELECT id, year, month, day, hour, minute, second, time_resolution "
						 "FROM treport_log_start "
						 "WHERE id_report = :id ;";
	QSqlQuery queryStart(*rep_database.database());
	if (!queryStart.prepare(qTextStart))
		return;
	queryStart.bindValue(":id", idReport);
	queryStart.exec();
	if (!queryStart.next())
		return;

	QSqlRecord recStart = queryStart.record();
	int idStart = recStart.value("id").toUInt();
	int year = recStart.value("year").toUInt();
	int month = recStart.value("month").toUInt();
	int day = recStart.value("day").toUInt();
	int hour = recStart.value("hour").toUInt();
	int minute = recStart.value("minute").toUInt();
	int second = recStart.value("second").toUInt();
	timeResolution = recStart.value("time_resolution").toUInt();
	dtStart.setDate(QDate(year, month, day));
	dtStart.setTime(QTime(hour, minute, second));

	// точки
	QString qTextPoint = "SELECT id_param, time, value "
						 "FROM treport_points "
						 "WHERE id_log_start = :id ;";
	QSqlQuery queryPoint(*rep_database.database());
	if (!queryPoint.prepare(qTextPoint))
		return;
	queryPoint.bindValue(":id", idStart);
	queryPoint.exec();

	while (queryPoint.next())
	{
		QSqlRecord recPoint = queryPoint.record();
		uint id = recPoint.value("id_param").toUInt();
		uint time = recPoint.value("time").toUInt();
		int value = recPoint.value("value").toInt();

		if (id > 5)
			continue;

		// время точки
		qint64 ms = dtStart.toMSecsSinceEpoch() + time * timeResolution;
		// значение точки
		double v = double(value);
		if (id <= 2)
			v = double(value) / 1000.;

		// расчет диапазона графика
		if (mapMin[id] > v)
			mapMin[id] = v;
		if (mapMax[id] < v)
			mapMax[id] = v;

		// добавляем точку
		mapSeries[id]->append(ms, v);

		// запоминаем последний параметр для повторной отрисовки
		if (1 == id)
			lastI = v;
		if (2 == id)
			lastU = v;

		// если параметр не пришел, то он не изменился
		if (1 != id && i_series->count() > 0 && ms != i_series->points().last().x())
			i_series->append(ms, lastI);
		if (2 != id && u_series->count() > 0 && ms != u_series->points().last().x())
			u_series->append(ms, lastU);
	}
	showChart();
}

void ZrmReportViewDialog::showChart()
{
	for (int i = 1; i <= 5; i++)
	{
		chart->addSeries(mapSeries[i]);
		mapSeries[i]->attachAxis(mapAxis[i]);
		mapSeries[i]->attachAxis(axisTime);

		// диапазон графика
		if (mapMin[i] == mapMax[i])
		{
			if (mapMin[i] >= 1)
				mapMin[i] -= 1;
			else
				mapMax[i] += 1;
		}
		mapAxis[i]->setRange(mapMin[i], mapMax[i]);
	}

	resetTime();
	frameButtons->setEnabled(true);
}

void ZrmReportViewDialog::resetTime()
{
	axisTime->setMin(dtStart);
	if (i_series->count() > 0)
	{
		qint64 ms = i_series->points().last().x();
		axisTime->setMax(QDateTime::fromMSecsSinceEpoch(ms));
	}
	else
		axisTime->setMax(dtStart.addSecs(60));
}

void ZrmReportViewDialog::scrollLeft()
{
	QDateTime dtStart = axisTime->min();
	QDateTime dtEnd = axisTime->max();
	qint64 ms = dtStart.msecsTo(dtEnd) / 10;
	axisTime->setMin(dtStart.addMSecs(-ms));
	axisTime->setMax(dtEnd.addMSecs(-ms));
}

void ZrmReportViewDialog::scrollRight()
{
	QDateTime dtStart = axisTime->min();
	QDateTime dtEnd = axisTime->max();
	qint64 ms = dtStart.msecsTo(dtEnd) / 10;
	axisTime->setMin(dtStart.addMSecs(ms));
	axisTime->setMax(dtEnd.addMSecs(ms));
}

void ZrmReportViewDialog::zoomIn()
{
	QDateTime dtStart = axisTime->min();
	QDateTime dtEnd = axisTime->max();
	qint64 ms = dtStart.msecsTo(dtEnd) / 10;
	axisTime->setMax(dtEnd.addMSecs(-ms));
}

void ZrmReportViewDialog::zoomOut()
{
	QDateTime dtStart = axisTime->min();
	QDateTime dtEnd = axisTime->max();
	qint64 ms = dtStart.msecsTo(dtEnd) / 10;
	axisTime->setMax(dtEnd.addMSecs(ms));
}
