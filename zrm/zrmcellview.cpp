﻿#include "zrmcellview.h"
#include <algorithm>
#include <QPalette>
#ifdef QT_DEBUG
    #include <QRandomGenerator>
#endif

ZrmCellView::ZrmCellView(QWidget* parent) :
    ZrmChannelWidget(parent)
{
    setupUi(this);
    connect(this->deltaU, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &ZrmCellView::deltaChanged);
    connect(this->deltaT, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &ZrmCellView::deltaChanged);
    initCellsTree();
}

void ZrmCellView::showDelta(bool show)
{
    deltaFrame->setVisible(show);
}

void ZrmCellView::channel_session(unsigned ch_num)
{
    if (m_source && m_channel == ch_num && m_source->channel_session(m_channel).is_active())
    {

        QSignalBlocker sb1(deltaT);
        QSignalBlocker sb2(deltaU);
        zrm::zrm_maskab_param_t dUT = m_source->channel_masakb_param(m_channel);
        deltaU->setValue(dUT.dU);
        deltaT->setValue(dUT.dT);

        zrm::params_t params;
        params.push_back(zrm::PARAM_CCNT);
        m_source->channel_subscribe_params(m_channel, params, true);

    }
}

void    ZrmCellView::onActivate()
{
    qDebug() << Q_FUNC_INFO;
    subscribeCells(true);
}

void    ZrmCellView::onDeactivate()
{
    qDebug() << Q_FUNC_INFO;
    subscribeCells(false);
}

void ZrmCellView::channel_param_changed(unsigned channel, const zrm::params_list_t& params_list)
{
    if (channel == m_channel)
    {
        zrm::params_list_t::const_iterator end = params_list.end();
        zrm::params_list_t::const_iterator ptr;

        if ( (ptr = params_list.find(zrm::PARAM_CCNT)) != end )
        {
#ifdef QT_DEBUG
            cellsCount(20);
#else
            cellsCount(ptr->second.value<uint16_t>(false));
#endif
        }

        if ((ptr = params_list.find(zrm::PARAM_CELL)) != end )
        {
            cellsParam();
        }
    }
    ZrmChannelWidget::channel_param_changed(channel, params_list);
}

class CellsStats
{
#if defined  Q_CC_MSVC
    double min_volt = DBL_MAX;
    double max_volt = DBL_MIN;
    double min_temp = DBL_MAX;
    double max_temp = DBL_MIN;

#else
    double min_volt = std::numeric_limits<double>::max();
    double max_volt = std::numeric_limits<double>::min();
    double min_temp = std::numeric_limits<double>::max();
    double max_temp = std::numeric_limits<double>::min();
#endif
    double mid_volt = 0;
    double mid_temp = 0;
    int    count = 0;
    double dU ;
    double dT ;
    double power = 1 ;
public:
    explicit CellsStats(double du, double dt, double pwr): dU(du), dT(dt), power(pwr) {};
    void   appendValue(double volt, double temp);
    void   stopAccum();
    double minVolt() const {return min_volt;}
    double maxVolt() const {return max_volt;}
    double midVolt() const {return mid_volt ;}
    double minTemp() const {return min_temp;}
    double maxTemp() const {return max_temp;}
    double midTemp() const {return mid_temp;}
    bool   isVoltOutBound(double value) const;
    bool   isTempOutBound(double value) const;
};

void   CellsStats::appendValue(double volt, double temp)
{
    ++count;
    min_volt = qMin(volt, min_volt);
    max_volt = qMax(volt, max_volt);
    mid_volt += pow(volt, power);

    min_temp = qMin(temp, min_temp);
    max_temp = qMax(temp, max_temp);
    mid_temp += pow(temp, power);
};

void   CellsStats::stopAccum()
{
    double cellsCount = (count < 2) ? 1 : count;
    double root = double(1.0) / power;
    mid_temp = pow(mid_temp / cellsCount, root);
    mid_volt = pow(mid_volt / cellsCount, root);

}

bool   CellsStats::isVoltOutBound(double value) const
{
    return !qFuzzyIsNull(dU) &&  fabs(mid_volt - value) > fabs(dU);
}

bool   CellsStats::isTempOutBound(double value) const
{
    return !qFuzzyIsNull(dT) &&  fabs(mid_temp - value) > fabs(dT);
}

#ifdef QT_DEBUG
namespace {
zrm::zrm_cells_t fakeCells(size_t sz)
{
    zrm::zrm_cells_t cells;
    cells.resize(sz);
    for (zrm::zrm_cell_t& cell : cells)
    {
        cell.m_volt = QRandomGenerator::global()->bounded(2000, 100000);
        cell.m_temp = QRandomGenerator::global()->bounded(5000, 30000);
    }
    return cells;
}
}
#endif

void ZrmCellView::setCellValues(int number, const zrm::zrm_cell_t& cell, const CellsStats& cs)
{
    QString volt = QString::number(cell.volt(), 'f', 2);
    QString temp = QString::number(cell.temp(), 'f', 2);
    bool voltOutBond = cs.isVoltOutBound(cell.volt());
    bool tempOutBond = cs.isTempOutBound(cell.volt());
    QColor colorU = (voltOutBond ? outBoundBackground : (number % 2) ? normalBackground : alterBackground);
    if (max_volt == cell.m_volt)
        colorU = colorMaxU;
    if (min_volt == cell.m_volt)
        colorU = colorMinU;
    QColor colorT = (tempOutBond ? outBoundBackground : (number % 2) ? normalBackground : alterBackground);
    if (max_temp == cell.m_temp)
        colorT = colorMaxT;

    if (m_orientation == Qt::Horizontal)
    {
        int column = number + 1;
        QTreeWidgetItem* voltItem = cellsTree->topLevelItem(HorizontalItemRoles::hVolt);
        QTreeWidgetItem* tempItem = cellsTree->topLevelItem(HorizontalItemRoles::hTemp);
        voltItem->setText(column, QString::number(cell.volt(), 'f', 2));
        voltItem->setData(column, Qt::BackgroundColorRole, colorU);
        voltItem->setData(column, Qt::TextColorRole, voltOutBond ? outBoundText : normalText);

        tempItem->setText(column, QString::number(cell.temp(), 'f', 2));
        tempItem->setData(column, Qt::BackgroundColorRole, colorT);
        tempItem->setData(column, Qt::TextColorRole, tempOutBond ? outBoundText : normalText);

    }
    else
    {
        QTreeWidgetItem* cellItem = cellsTree->topLevelItem(number);
        cellItem->setText(VerticalColumnRoles::vVolt, volt);
        cellItem->setData(VerticalColumnRoles::vVolt, Qt::BackgroundColorRole, colorU);
        cellItem->setData(VerticalColumnRoles::vVolt, Qt::TextColorRole, voltOutBond ? outBoundText : normalText);

        cellItem->setText(VerticalColumnRoles::vTemp, temp);
        cellItem->setData(VerticalColumnRoles::vTemp, Qt::BackgroundColorRole, colorT);
        cellItem->setData(VerticalColumnRoles::vTemp, Qt::TextColorRole, tempOutBond ? outBoundText : normalText);
    }
}


void ZrmCellView::subscribeCells(bool subscribe)
{
    if (!m_source)
        return;

    zrm::params_t params;
    params.push_back(zrm::PARAM_CELL);
    m_source->channel_subscribe_params(m_channel, params, subscribe);

    if (subscribe)
        m_source->channel_query_params(m_channel, params);

}

void ZrmCellView::cellsParam()
{
    cellsTree->setUpdatesEnabled(false);
    zrm::zrm_cells_t cells;
#ifdef QT_DEBUG
    cells = fakeCells(20);
#else
    cells  =  m_source->channel_cell_info(m_channel);
#endif
    cellsCount(static_cast<uint16_t>(cells.size()));

    zrm::zrm_maskab_param_t dUT = m_source->channel_masakb_param(m_channel);

    CellsStats cs(dUT.dU, dUT.dU, 2.0);
    min_volt = UINT32_MAX;
    max_volt = 0;
    max_temp = 0;

    for (const zrm::zrm_cell_t& cell : cells)
    {
        cs.appendValue(cell.volt(), cell.temp());
        if (min_volt > cell.m_volt)
            min_volt = cell.m_volt;
        if (max_volt < cell.m_volt)
            max_volt = cell.m_volt;
        if (max_temp < cell.m_temp)
            max_temp = cell.m_temp;
    }

    cs.stopAccum();

    int number = 0;
    for (const zrm::zrm_cell_t& cell : cells)
    {
        setCellValues(number++, cell, cs);
    }
    cellsTree->setUpdatesEnabled(true);
}

void ZrmCellView::update_controls()
{
    ZrmChannelWidget::update_controls();
    if (m_source && m_channel)
    {
        cellsParam();
        channel_session(m_channel);
        channel_param_changed(m_channel, m_source->channel_params(m_channel));
    }
}

void ZrmCellView::clear_controls()
{
    cellsCount(0);
}

void ZrmCellView::resizeEvent(QResizeEvent* re)
{
    ZrmChannelWidget::resizeEvent(re);
    updateColumnWidth();
}

void ZrmCellView::showEvent(QShowEvent* se)
{
    ZrmChannelWidget::showEvent(se);
    updateColumnWidth();
}

void ZrmCellView::initCellsTree()
{
    normalBackground = cellsTree->palette().color(QPalette::Background);
    alterBackground = QColor(211, 211, 211);
    normalText = cellsTree->palette().color(QPalette::Text);
    outBoundBackground = Qt::GlobalColor::darkRed;
    outBoundText = Qt::GlobalColor::white;

    makeHorizontalCells();
    QHeaderView* hv = cellsTree->header();
    hv->setSectionResizeMode(QHeaderView::ResizeMode::Fixed);
    hv->setDefaultAlignment(Qt::AlignmentFlag::AlignCenter);
    updateColumnWidth();
}

void ZrmCellView::updateColumnWidth()
{
    QHeaderView* hv = cellsTree->header();
    if (!hv->count())
        return;

    int colWidth = hv->width() / hv->count();

    for (int index = 0; index < hv->count(); index++)
    {
        hv->resizeSection(index, colWidth);
    }
}

void ZrmCellView::cellsCount(uint16_t ccnt)
{
    QString emptyStr("--,--");
    bool needUpdate = false;

    if (m_orientation == Qt::Vertical)
    {

        uint16_t itemsCount = cellsTree->topLevelItemCount();
        needUpdate = itemsCount != ccnt;

        while (itemsCount < ccnt  )
        {
            QTreeWidgetItem* item = new QTreeWidgetItem(cellsTree);
            item->setText(VerticalColumnRoles::vNumber, QString::number(++itemsCount));
            item->setText(VerticalColumnRoles::vVolt, emptyStr);
            item->setText(VerticalColumnRoles::vTemp, emptyStr);
            for (int i = 0; i <= VerticalColumnRoles::vTemp; i++)
            {
                item->setTextAlignment(i, Qt::AlignmentFlag::AlignCenter );
            }
        }

        while (itemsCount > ccnt )
        {
            delete cellsTree->topLevelItem(--itemsCount);
        }

        int cCount = ccnt;
        if (cCount)
        {
            QTreeWidgetItem* item = cellsTree->topLevelItem(0);
            QSize itemSize =  cellsTree->visualItemRect(item).size();
            if (cCount > 20)
                cCount = 20;
            cellsTree->setMinimumHeight(itemSize.height() * cCount + cellsTree->header()->height() + 2);
        }
        else
            cellsTree->setMinimumHeight(cellsTree->header()->height() + 22);
    }
    else
    {
        uint16_t itemsCount = cellsTree->columnCount();
        ++ccnt;
        needUpdate = itemsCount != ccnt;
        cellsTree->setColumnCount(ccnt);

        while (itemsCount < ccnt )
        {
            cellsTree->headerItem()->setText(itemsCount, QString::number(itemsCount));
            QTreeWidgetItem* item = cellsTree->topLevelItem(HorizontalItemRoles::hVolt);
            item->setText(itemsCount, emptyStr);
            item->setTextAlignment(itemsCount, Qt::AlignmentFlag::AlignCenter );

            item = cellsTree->topLevelItem(HorizontalItemRoles::hTemp);
            item->setText(itemsCount, emptyStr);
            item->setTextAlignment(itemsCount, Qt::AlignmentFlag::AlignCenter );
            ++itemsCount;

        }
    }
    if (needUpdate)
    {
        updateColumnWidth();
    }
}

int  ZrmCellView::getRowsCount() const
{
    return cellsTree->topLevelItemCount() + 1;
}

QSize ZrmCellView::sizeHint() const
{
    QSize sz = ZrmChannelWidget::sizeHint();
    int cCount = cellsTree->topLevelItemCount();
    if (cCount && m_orientation == Qt::Vertical)
    {
        QTreeWidgetItem* item = cellsTree->topLevelItem(0);
        QSize itemSize =  cellsTree->visualItemRect(item).size();
        if (cCount > 20)
            cCount = 20;
        sz.setHeight(itemSize.height() * cCount + cellsTree->header()->height());
    }
    return sz;
}

void ZrmCellView::setOrientation(Qt::Orientation orientation)
{
    if (orientation != m_orientation)
    {
        m_orientation = orientation;
        if (orientation == Qt::Orientation::Horizontal)
            makeHorizontalCells();
        else
            makeVerticalCells();
        if (m_source)
            cellsParam();
    }
}

void ZrmCellView::makeHorizontalCells()
{
    cellsTree->setColumnCount(1);
    cellsTree->clear();
    QTreeWidgetItem* item;
    item = new QTreeWidgetItem(cellsTree, QStringList() << voltText);
    item->setTextAlignment(0, Qt::AlignmentFlag::AlignCenter );
    cellsTree->addTopLevelItem( item);

    item = new QTreeWidgetItem(cellsTree, QStringList() << tempText);
    item->setTextAlignment(0, Qt::AlignmentFlag::AlignCenter );
    cellsTree->addTopLevelItem( item);

    item = cellsTree->headerItem();
    item->setText(VerticalColumnRoles::vNumber, numberText);
    item->setTextAlignment(0, Qt::AlignmentFlag::AlignCenter );
}

void ZrmCellView::makeVerticalCells()
{
    cellsTree->setColumnCount(3);
    cellsTree->clear();
    QTreeWidgetItem* item = cellsTree->headerItem();
    item->setTextAlignment(VerticalColumnRoles::vNumber, Qt::AlignmentFlag::AlignCenter );
    item->setText(VerticalColumnRoles::vNumber, numberText);
    item->setTextAlignment(VerticalColumnRoles::vVolt, Qt::AlignmentFlag::AlignCenter );
    item->setText(VerticalColumnRoles::vVolt, voltText);
    item->setTextAlignment(VerticalColumnRoles::vTemp, Qt::AlignmentFlag::AlignCenter );
    item->setText(VerticalColumnRoles::vTemp, tempText);
}

void ZrmCellView::deltaChanged (double)
{
    zrm::zrm_maskab_param_t _map;
    _map.dU = deltaU->value();
    _map.dT = deltaT->value();
    m_source->channel_set_masakb_param(m_channel, _map);

}
