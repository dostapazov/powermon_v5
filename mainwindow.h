﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenu>
#include "ui_mainwindow.h"
#include <zrmchannelmimimal.h>
#include <qscreen.h>
#include <utils/localedotreplacer.h>

class MainWindow : public QMainWindow, protected Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow() override;

public slots:
    void showMainWindow(bool maximized);
    void showWindowMaximized();

private slots:

    void channel_activated(ZrmChannelMimimal* cm, bool bSelect);
    void action_toggled(bool checked);
    void set_style(const QString&   styleName);
    void edit_font_changed(const QFont& font);
    void edit_font_changed_props();
    void configure_apply();

    void onStyleToogled(bool checked);
    void onLoginToogled(bool checked);

private:
    bool event(QEvent* event) override;
    void actionConfigure(bool checked);
    void actionMethodEditor(bool checked);
    void actionDevMethod(bool checked);
    QFont edit_font  (const QFont& f);
    void init_styles ();
    void init_actions();
    void init_slots  ();
    void install_event_filers();

    //void configure_apply();
    void style_apply    ();

    static QString connectivity_file_name();
    static QString window_param_file_name();


    void set_font_for_edit ();

    void writeConfig       ();
    void readConfig        ();
    void set_default_config ();
#ifdef Q_OS_ANDROID
    void update_android_ui();
#else
    void update_desktop_ui();
#endif
    void update_ui();
    void setupStyleSheet();

    QActionGroup*       m_action_grp     = Q_NULLPTR;
    static QtMessageHandler   prev_msg_handler;
    static void msg_handler   (QtMsgType msg_type, const QMessageLogContext& msg_context, const QString& msg_text);
    void setupActions();
    void updateFont(const QFont& fnt);

    LocaleDotReplacer  dotReplacer;
    void setupWindowFlags();
    void updateStatusBar(ZrmChannelMimimal* cm, zrm::ZrmConnectivity* conn, uint16_t channel);
    void initApp();
    void initTitle(QString user);
    void initConnectivity();
};

#endif // MAINWINDOW_H
