#ifndef USEREDITDIALOG_H
#define USEREDITDIALOG_H

#include "ui_UserEditDialog.h"

class QSqlRecord;
class QSqlTableModel;

class UserEditDialog : public QDialog, private Ui::UserEditDialog
{
    Q_OBJECT

public:
    explicit UserEditDialog(QWidget *parent = nullptr);

    void setData(QSqlRecord* rec);
    void getData(QSqlTableModel* model, int row);

public slots:
    virtual void accept() override;
};

#endif // USEREDITDIALOG_H
