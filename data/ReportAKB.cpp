﻿#include "ReportAKB.h"

#include <QSqlQuery>
#include <QSqlRecord>

ReportAKB::ReportAKB(QWidget* parent) :
    QWidget(parent)
{
    setupUi(this);
    init_actions();

    open_types();
    open_numbers();
}

ReportAKB::~ReportAKB()
{
    NumbersTable->setModel(nullptr);
    TypesTable->setModel(nullptr);
}

void ReportAKB::init_actions()
{
    connect(actTypeNew, &QAction::triggered, this, &ReportAKB::typesCreate);
    connect(actTypeMarkDel, &QAction::triggered, this, &ReportAKB::typesMarkDel);

    bTypesNew->setDefaultAction(actTypeNew);
    bTypesMarkDel->setDefaultAction(actTypeMarkDel);

    connect(actNumberNew, &QAction::triggered, this, &ReportAKB::numbersCreate);
    connect(actNumberMarkDel, &QAction::triggered, this, &ReportAKB::numbersMarkDel);

    bNumbersNew->setDefaultAction(actNumberNew);
    bNumbersMarkDel->setDefaultAction(actNumberMarkDel);
}

void ReportAKB::open_types()
{
    if (!TypesTable->model())
    {
#ifdef Q_OS_ANDROID
        TypesTable->setEditTriggers(QAbstractItemView::EditTrigger::AllEditTriggers);
#endif
        TypesTable->setCornerButtonEnabled(false);
        rep_database.assign_types_model(TypesTable);
        auto hdr = TypesTable->horizontalHeader();
        hdr->setSectionResizeMode(QHeaderView::ResizeMode::ResizeToContents);
        hdr->setSectionResizeMode(rep_database.field_index(TypesTable->model(), "name"), QHeaderView::ResizeMode::Stretch);
        connect(rep_database.types_model(), &QSqlTableModel::dataChanged, this, &ReportAKB::typesApply);
    }
}

void ReportAKB::typesMarkDel()
{
    QModelIndex index = TypesTable->currentIndex();
    if (index.isValid())
        rep_database.mark_del(TypesTable->model(), index.row());
}

void ReportAKB::typesCreate()
{
    rep_database.new_record(TypesTable->model(), TypesTable);
}

void ReportAKB::typesApply()
{
    rep_database.submit(TypesTable->model());
}

void ReportAKB::numbersMarkDel()
{
    QModelIndex index = NumbersTable->currentIndex();
    if (index.isValid())
        rep_database.mark_del(NumbersTable->model(), index.row());
}

void ReportAKB::numbersCreate()
{
    QAbstractItemModel* model = NumbersTable->model();
    QVariant typeId = recordId(TypesTable);

    if (!typeId.isNull())
    {
        int row = rep_database.new_record(model, NumbersTable);
        model->setData(model->index(row, rep_database.field_index(model, "id_type")), typeId );
    }
}

void ReportAKB::numbersApply()
{
    rep_database.submit(NumbersTable->model());
}

QVariant ReportAKB::recordId(QTableView* table)
{
    QAbstractItemModel* model = table->model();
    QModelIndex   recId = model->index(table->currentIndex().row(), rep_database.field_id(model));
    return recId.isValid() ? recId.data() : QVariant();
}

void ReportAKB::open_numbers()
{
    if (!NumbersTable->model())
    {
        NumbersTable->setCornerButtonEnabled(false);
        rep_database.assign_numbers_model(NumbersTable);
        connect(rep_database.numbers_model(), &QSqlTableModel::dataChanged, this, &ReportAKB::numbersApply);

#ifdef Q_OS_ANDROID
        NumbersTable->setEditTriggers(QAbstractItemView::EditTrigger::AllEditTriggers);
#endif
        NumbersTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        QItemSelectionModel* selection_model = TypesTable->selectionModel();
        if (selection_model)
        {
            connect(selection_model, &QItemSelectionModel::currentRowChanged, this, &ReportAKB::type_row_changed);
        }
        selection_model = NumbersTable->selectionModel();
        if (selection_model)
        {
            connect(selection_model, &QItemSelectionModel::currentRowChanged, this, &ReportAKB::number_row_changed);
        }
    }
}

void ReportAKB::type_row_changed(const QModelIndex& current)
{
    auto model = current.model();
    QVariant type_id = model->index(current.row(), rep_database.field_id(model)).data();
    rep_database.numbers_select(type_id);
    model = rep_database.numbers_model();
    if (model->rowCount() )
    {
        NumbersTable->setCurrentIndex(model->index(0, 2));
    }
}

void ReportAKB::number_row_changed(const QModelIndex& current)
{
    //Выбран другой аккумулятор
    Q_UNUSED(current)
}

void ReportAKB::setIDAKB(qlonglong idAKB)
{
    if (idAKB > 0)
    {
        QString query_text = QString("SELECT id_type FROM tbattery_list WHERE id = %1 ").arg(idAKB);
        QSqlQuery query(*rep_database.database());
        query.prepare(query_text);
        query.exec();
        query.next();
        QVariant idType = query.value(0);
        QSqlTableModel* tm = rep_database.types_model();
        for (int i = 0; i < tm->rowCount(); i++)
        {
            QSqlRecord rec = tm->record(i);
            if (rec.value("id") == idType)
            {
                TypesTable->setCurrentIndex(TypesTable->model()->index(i, 1));
                rep_database.numbers_select(idType);
                QSqlTableModel* nm = rep_database.numbers_model();
                for (int j = 0; j < nm->rowCount(); j++)
                {
                    QSqlRecord rec = nm->record(j);
                    if (rec.value("id").toLongLong() == idAKB)
                        NumbersTable->setCurrentIndex(NumbersTable->model()->index(j, 2));
                }
                break;
            }
        }
    }
}

qlonglong ReportAKB::getIDAKB()
{
    qlonglong idAKB = 0;
    QModelIndex index = NumbersTable->currentIndex();
    if (index.isValid())
    {
        auto model = index.model();
        idAKB = model->index(index.row(), rep_database.field_id(model)).data().toLongLong();
    }
    return idAKB;
}
