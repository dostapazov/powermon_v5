#include "AndOrButton.h"

AndOrButton::AndOrButton(QWidget *parent) : QPushButton(parent)
{
    connect(this, SIGNAL(clicked()), this, SLOT(onClick()));
}

void AndOrButton::setState(State s)
{
    state = s;
    onStateChanged();
}

void AndOrButton::onClick()
{
    switch (state)
    {
    case EMPTY :
        state = OR;
        break;
    case OR :
        state = AND;
        break;
    case AND :
        state = EMPTY;
        break;
    }

    onStateChanged();

    emit stateChanged(state);
}

void AndOrButton::onStateChanged()
{
    switch (state)
    {
    case OR :
        setText("ИЛИ");
        break;
    case AND :
        setText("И");
        break;
    default :
        setText("-");
        break;
    }
}
