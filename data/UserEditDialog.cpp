#include "UserEditDialog.h"

#include <QSqlRecord>
#include <QSqlTableModel>
#include <QMessageBox>

#include "MD5.h"

UserEditDialog::UserEditDialog(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
}

void UserEditDialog::setData(QSqlRecord *rec)
{
    lineEditSurname->setText(rec->value("sname").toString());
    lineEditName->setText(rec->value("fname").toString());
    lineEditPatronymic->setText(rec->value("pname").toString());
    lineEditLogin->setText(rec->value("login").toString());
    checkBoxAdmin->setChecked(rec->value("admin").toBool());
}

void UserEditDialog::getData(QSqlTableModel* model, int row)
{
    model->setData(model->index(row, model->fieldIndex("sname")), lineEditSurname->text());
    model->setData(model->index(row, model->fieldIndex("fname")), lineEditName->text());
    model->setData(model->index(row, model->fieldIndex("pname")), lineEditPatronymic->text());
    model->setData(model->index(row, model->fieldIndex("login")), lineEditLogin->text());
    if (!lineEditPassword->text().isEmpty())
        model->setData(model->index(row, model->fieldIndex("password")), makeMD5(lineEditPassword->text()));
    model->setData(model->index(row, model->fieldIndex("admin")), checkBoxAdmin->isChecked());
}

void UserEditDialog::accept()
{
    if (lineEditPassword->text() != lineEditPassword2->text())
    {
        QMessageBox::warning(this, "Внимание!", "Подтверждение пароля не соответствует");
        return;
    }
    QDialog::accept();
}
