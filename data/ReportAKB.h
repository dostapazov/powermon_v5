﻿#ifndef REPORTAKB_H
#define REPORTAKB_H

#include "ui_ReportAKB.h"

#include "zrmreportdatabase.h"

class ReportAKB : public QWidget, private Ui::ReportAKB
{
    Q_OBJECT

public:
    explicit ReportAKB(QWidget* parent = nullptr);
    ~ReportAKB() override;

    void setIDAKB(qlonglong idAKB);
    qlonglong getIDAKB();

private slots:
    void typesMarkDel();
    void typesCreate();
    void typesApply();

    void numbersMarkDel();
    void numbersCreate();
    void numbersApply();

    void type_row_changed(const QModelIndex& current);
    void number_row_changed(const QModelIndex& current);

private :
    QVariant recordId(QTableView* table);
    void init_actions();
    void open_types();
    void open_numbers();

private:
    ZrmReportDatabase rep_database;
};

#endif // REPORTAKB_H
