﻿#include "reportcommon.h"

#include <qdebug.h>
#include <qsqlquery.h>
#include <qitemdelegate.h>
#include <zrm_connectivity.hpp>
#include <zrmbasewidget.h>
#include <qpainter.h>
#include <QMessageBox>
#include <QShortcut>
#include <QSqlRecord>

#include "ZrmReportViewDialog.h"
#include "UserEditDialog.h"

QVariant ReportDetailsModel::data(const QModelIndex& index, int role) const
{
    QVariant value = QSqlQueryModel::data(index, role);
    if (index.column() == 1 && role == Qt::DisplayRole)
    {
        value = pwm_utils::hms2string(pwm_utils::secunds2hms(value.toUInt()), true);
    }
    return value;
}

class ReportItemDelegate: public QItemDelegate
{
public:
    ReportItemDelegate(QObject* parent = Q_NULLPTR): QItemDelegate(parent) {}
    void paint(QPainter* painter,
               const QStyleOptionViewItem& option,
               const QModelIndex& index) const override;
private:
    void custom_paint(QPainter* painter,
                      const QStyleOptionViewItem& option,
                      const QString& str
                     ) const;
};

void ReportCommon::makeShortCuts()
{
    QShortcut* sc = new QShortcut(this);
    sc->setKey(QKeySequence("Ctrl+Ins"));
    connect(sc, &QShortcut::activated, this, &ReportCommon::createRecord);
    sc  = new QShortcut(this);
    sc->setKey(QKeySequence("Ctrl+Del"));
    connect(sc, &QShortcut::activated, this, &ReportCommon::markDelRecord);
}

void ReportCommon::initUi()
{
    tabWidget->setCurrentIndex(0);
    splitter->setStretchFactor  (0, 1);
    splitter->setStretchFactor  (1, 3);
    splitter_2->setStretchFactor(0, 2);
    splitter_2->setStretchFactor(1, 1);
    ZrmBaseWidget::setWidgetsShadow<QToolButton>(gb_akb_type, 4, 4);
    ZrmBaseWidget::setWidgetsShadow<QToolButton>(gb_akb_list, 4, 4);
    ZrmBaseWidget::setWidgetsShadow<QToolButton>(fr_users_btn, 4, 4);
    QDate cdt = QDate::currentDate();
    dtm_beg->setDate(cdt.addMonths(-1));
    dtm_end->setDate(cdt);
    ReportTable->setItemDelegate( new ReportItemDelegate(ReportTable));
}

void ReportCommon::makeSlotConnections()
{
    connect(tabWidget, &QTabWidget::currentChanged, this, &ReportCommon::switch_pages);
    connect(bAllTimes, &QAbstractButton::clicked, this, &ReportCommon::read_reports);
}

ReportCommon::ReportCommon(QWidget* parent) :
    QWidget(parent)
{
    setupUi(this);
    initUi();
    init_actions();

    open_types();
    open_numbers();
    updateReportButtons();

    makeShortCuts();
    makeSlotConnections();
}

ReportCommon::~ReportCommon()
{
    UsersTable->setModel(Q_NULLPTR);
    NumbersTable->setModel(Q_NULLPTR);
    TypesTable->setModel(Q_NULLPTR);
}

void ReportCommon::init_actions()
{
    connect(actUserNew, &QAction::triggered, this, &ReportCommon::userCreate);
    connect(actUserEdit, &QAction::triggered, this, &ReportCommon::userEdit);
    connect(actUserApply, &QAction::triggered, this, &ReportCommon::userApply);
    connect(actUserRevert, &QAction::triggered, this, &ReportCommon::userRevert);
    connect(actUserMarkDel, &QAction::triggered, this, &ReportCommon::userMarkDel);

    bUserNew    ->setDefaultAction(actUserNew);
    bUserEdit   ->setDefaultAction(actUserEdit);
    bUserMarkDel->setDefaultAction(actUserMarkDel);
    //    bUserApply  ->setDefaultAction(actUserApply);
    //    bUserRevert ->setDefaultAction(actUserRevert);


    connect(actTypeApply, &QAction::triggered, this, &ReportCommon::typesApply);
    connect(actTypeRevert, &QAction::triggered, this, &ReportCommon::typesRevert);
    connect(actTypeNew, &QAction::triggered, this, &ReportCommon::typesCreate);
    connect(actTypeMarkDel, &QAction::triggered, this, &ReportCommon::typesMarkDel);

    bTypesNew      ->setDefaultAction(actTypeNew);
    bTypesMarkDel  ->setDefaultAction(actTypeMarkDel);
    //    bTypesApply    ->setDefaultAction(actTypeApply);
    //    bTypesRevert   ->setDefaultAction(actTypeRevert);

    connect(actNumberNew, &QAction::triggered, this, &ReportCommon::numbersCreate);
    connect(actNumberApply, &QAction::triggered, this, &ReportCommon::numbersApply);
    connect(actNumberRevert, &QAction::triggered, this, &ReportCommon::numbersRevert);
    connect(actNumberMarkDel, &QAction::triggered, this, &ReportCommon::numbersMarkDel);


    bNumbersNew    ->setDefaultAction(actNumberNew);
    bNumbersMarkDel->setDefaultAction(actNumberMarkDel);
    //    bNumbersApply  ->setDefaultAction(actNumberApply);
    //    bNumbersRevert ->setDefaultAction(actNumberRevert);

    connect(actRefreshReports, &QAction::triggered, this, &ReportCommon::read_reports);
    connect(actDeleteReport, &QAction::triggered, this, &ReportCommon::deleteReport);
    connect(actShowReport, &QAction::triggered, this, &ReportCommon::showReport);

    bReadReports->setDefaultAction(actRefreshReports);
    bDelReport->setDefaultAction(actDeleteReport);
    bShowReport->setDefaultAction(actShowReport);
}

void ReportCommon::switch_pages(int index)
{
    if (index == 1  )
    {
        open_users();
    }
}

void ReportCommon::open_users  ()
{
    if (!UsersTable->model())
    {
        UsersTable->setCornerButtonEnabled(false);
        rep_database.assign_users_model(UsersTable);
        UsersTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

        QItemSelectionModel* selection_model = UsersTable->selectionModel();
        if (selection_model)
            connect(selection_model, &QItemSelectionModel::currentRowChanged, this, &ReportCommon::user_row_changed);

        setAdmin(bAdmin);
    }
}

void ReportCommon::user_row_changed(const QModelIndex& current)
{
    bool bEnable = (bAdmin && current.isValid());
    actUserEdit->setEnabled(bEnable);
    actUserMarkDel->setEnabled(bEnable);
}

void ReportCommon::open_types  ()
{
    if (!TypesTable->model())
    {
#ifdef Q_OS_ANDROID
        TypesTable->setEditTriggers(QAbstractItemView::EditTrigger::AllEditTriggers);
#endif
        TypesTable->setCornerButtonEnabled(false);
        rep_database.assign_types_model(TypesTable);
        auto hdr = TypesTable->horizontalHeader();
        hdr->setSectionResizeMode(QHeaderView::ResizeMode::ResizeToContents);
        hdr->setSectionResizeMode(rep_database.field_index(TypesTable->model(), "name"), QHeaderView::ResizeMode::Stretch);
        connect(rep_database.types_model(), &QSqlTableModel::dataChanged, this, &ReportCommon::typesApply);
    }
}

void ReportCommon::userMarkDel()
{
    QModelIndex index = UsersTable->currentIndex();
    if (index.isValid())
        rep_database.mark_del(UsersTable->model(), index.row());
}

void ReportCommon::userCreate()
{
    UserEditDialog dlg(this);
    if (QDialog::Accepted == dlg.exec())
    {
        QSqlTableModel* model = rep_database.users_model();
        int row = model->rowCount();
        if (model->insertRow(row))
        {
            dlg.getData(model, row);
            if (model->submit())
                UsersTable->setCurrentIndex(model->index(row, 1));
            else
                model->revert();
        }
    }
}

void ReportCommon::userEdit()
{
    QModelIndex index = UsersTable->currentIndex();
    if (!index.isValid())
        return;
    QSqlTableModel* model = rep_database.users_model();
    QSqlRecord rec = model->record(index.row());
    UserEditDialog dlg(this);
    dlg.setData(&rec);
    if (QDialog::Accepted == dlg.exec())
    {
        dlg.getData(model, index.row());
        if (!model->submit())
            model->revert();
    }
}

void ReportCommon::userApply  ()
{
    rep_database.submit(UsersTable->model());
}

void ReportCommon::userRevert ()
{
    rep_database.revert(UsersTable->model());
}

void ReportCommon::typesMarkDel()
{
    QModelIndex index = TypesTable->currentIndex();
    if (index.isValid())
        rep_database.mark_del(TypesTable->model(), index.row());
}

void ReportCommon::typesCreate()
{
    rep_database.new_record(TypesTable->model(), TypesTable);
}

void ReportCommon::typesApply()
{
    rep_database.submit(TypesTable->model());
}

void ReportCommon::typesRevert()
{
    rep_database.revert(TypesTable->model());
}

void ReportCommon::numbersMarkDel()
{
    QModelIndex index = NumbersTable->currentIndex();
    if (index.isValid())
        rep_database.mark_del(NumbersTable->model(), index.row());

}

void ReportCommon::numbersCreate()
{
    QAbstractItemModel* model = NumbersTable->model();
    QVariant typeId = recordId(TypesTable);

    if (!typeId.isNull())
    {
        int row = rep_database.new_record(model, NumbersTable);
        model->setData(model->index(row, rep_database.field_index(model, "id_type")), typeId );
    }
}

void ReportCommon::numbersApply()
{
    rep_database.submit(NumbersTable->model());
}

void ReportCommon::numbersRevert()
{
    rep_database.revert(NumbersTable->model());
}

QVariant ReportCommon::recordId(QTableView* table)
{
    QAbstractItemModel* model = table->model();
    QModelIndex   recId = model->index(table->currentIndex().row(), rep_database.field_id(model));
    return recId.isValid() ? recId.data() : QVariant();
}

void ReportCommon::open_numbers()
{
    if (!NumbersTable->model())
    {
        NumbersTable->setCornerButtonEnabled(false);
        rep_database.assign_numbers_model(NumbersTable);
        connect(rep_database.numbers_model(), &QSqlTableModel::dataChanged, this, &ReportCommon::numbersApply);

#ifdef Q_OS_ANDROID
        NumbersTable->setEditTriggers(QAbstractItemView::EditTrigger::AllEditTriggers);
#endif
        NumbersTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        QItemSelectionModel* selection_model = TypesTable->selectionModel();
        if (selection_model)
        {
            connect(selection_model, &QItemSelectionModel::currentRowChanged, this, &ReportCommon::type_row_changed);
        }
        selection_model = NumbersTable->selectionModel();
        if (selection_model)
        {
            connect(selection_model, &QItemSelectionModel::currentRowChanged, this, &ReportCommon::number_row_changed);
        }
    }
}

void ReportCommon::type_row_changed(const QModelIndex& current)
{
    auto model = current.model();
    QVariant type_id = model->index(current.row(), rep_database.field_id(model)).data();
    rep_database.numbers_select(type_id);
    model = rep_database.numbers_model();
    if (model->rowCount() )
    {
        NumbersTable->setCurrentIndex(model->index(0, 2));
    }
    else
        read_reports();
}

void ReportCommon::number_row_changed(const QModelIndex& current)
{
    //Выбран другой аккумулятор
    Q_UNUSED( current )
    read_reports();

}


void ReportCommon::report_query_bind_values(QSqlQuery& query)
{
    int number_id = rep_database.get_record_id(NumbersTable->model(), NumbersTable->currentIndex().row());
    query.bindValue(":id_battery", number_id);
    if (!bAllTimes->isChecked())
    {
        QTime time(0, 0, 0);
        QDateTime dtm(dtm_beg->date(), time);
        query.bindValue(":dtm_beg", dtm);
        dtm = QDateTime(dtm_end->date(), time).addDays(1);
        query.bindValue(":dtm_end", dtm);
    }

}

QSqlQuery ReportCommon::report_query_get()
{
    QString qtext =
        "select "
        "r.id, r.id_battery, r.id_user, cast(r.dtm as text) dtm "
        ",u.short_fio, r.total_duration, r.total_energy, r.total_capacity "
        "from treport r "
        "left join tusers u on u.id = r.id_user "
        "where r.id_battery = :id_battery ";
    if (!bAllTimes->isChecked())
        qtext += QString("and (r.dtm >= :dtm_beg and r.dtm <=  :dtm_end )");
    qtext +=  QString("order by r.dtm ");
    ;
    QSqlQuery query(*rep_database.database());
    if (query.prepare(qtext))
    {
        report_query_bind_values( query);
        query.exec();
    }
    return  query;
}

void ReportCommon::initReportsModel()
{
    m_reports_model = new QSqlQueryModel(this);
    ReportTable->setModel(m_reports_model);
    m_reports_model->setQuery(report_query_get());

    auto hdr = ReportTable->horizontalHeader();
    hdr->setSectionResizeMode(QHeaderView::ResizeMode::ResizeToContents);
    hdr->setSectionResizeMode(4, QHeaderView::ResizeMode::Stretch);
    hdr->hideSection(TREPORT_FIELDS::id_rep);
    hdr->hideSection(TREPORT_FIELDS::id_bat);
    hdr->hideSection(TREPORT_FIELDS::id_user);

    m_reports_model->setHeaderData(TREPORT_FIELDS::timestamp, Qt::Horizontal, QObject::tr("Дата"));
    m_reports_model->setHeaderData(TREPORT_FIELDS::user_fio, Qt::Horizontal, QObject::tr("Пользователь"));
    m_reports_model->setHeaderData(TREPORT_FIELDS::duration, Qt::Horizontal, QObject::tr("Длительность"));
    m_reports_model->setHeaderData(TREPORT_FIELDS::energy, Qt::Horizontal, QObject::tr("Ёмкость"));
    m_reports_model->setHeaderData(TREPORT_FIELDS::capacity, Qt::Horizontal, QObject::tr("Ёмкость АКБ"));
    auto selection_model = ReportTable->selectionModel();
    if (selection_model)
    {
        connect(selection_model, &QItemSelectionModel::currentRowChanged, this, &ReportCommon::report_row_changed);
        connect(selection_model, &QItemSelectionModel::currentRowChanged, this, &ReportCommon::updateReportButtons);
    }
    updateReportButtons();
}

void ReportCommon::read_reports    ()
{
    if (!m_reports_model)
    {
        initReportsModel();
    }
    else
    {
        m_reports_model->setQuery(report_query_get());
    }
    report_row_changed(m_reports_model->index(0, 0));

}

void ReportCommon::showReport()
{
    int row = ReportTable->currentIndex().row();
    if (row < 0 || !m_reports_model)
        return;
    qlonglong rep_id =  m_reports_model->index(row, 0).data().toLongLong();
    ZrmReportViewDialog report(this);
    report.setReportId(rep_id);

    // размеры окна подгоняем под гравное окно без рамки
    QObject* p = parent();
    bool bContinue = true;
    while (p && bContinue)
    {
        ZrmGroupWidget* cw = dynamic_cast<ZrmGroupWidget*>(p);
        if (cw)
        {
            QRect rect = cw->geometry();
            rect.setWidth(rect.width() - 10);
            rect.setHeight(rect.height() - 20);
            rect.moveCenter(cw->mapToGlobal(cw->geometry().center()));
            report.setGeometry(rect);
            bContinue = false;
        }
        p = p->parent();
    }

    report.exec();
}

void ReportCommon::deleteReport()
{

    int row = ReportTable->currentIndex().row();
    if (row < 0 || !m_reports_model)
        return;
    if (QMessageBox::Yes != QMessageBox::question(this, "Удаление", "Удалить выбранный отчет?"))
        return;
    QVariant rep_id =  m_reports_model->index(row, 0).data();
    rep_database.reportDelete(rep_id);
    read_reports();
    updateReportButtons();
}

void ReportCommon::updateReportButtons()
{
    bool bEnable = ReportTable->currentIndex().isValid();
    bShowReport->setEnabled(bEnable);
    bDelReport->setEnabled(bEnable);
}

void ReportItemDelegate::custom_paint(QPainter* painter, const QStyleOptionViewItem& option, const QString& str  ) const
{
    painter->save();
    if (option.state & QStyle::State_Selected)
    {
        painter->fillRect(option.rect, option.palette.highlight());
        painter->setBrush(option.palette.highlightedText());
        painter->setPen(option.palette.highlightedText().color());
    }
    painter->drawText(painter->boundingRect(option.rect, Qt::AlignHCenter | Qt::AlignVCenter, str), str);
    painter->restore();

}


void ReportItemDelegate::paint(QPainter* painter,
                               const QStyleOptionViewItem& option,
                               const QModelIndex& index) const
{
    /*Отрисовка элементов отчета*/
    switch (index.column())
    {

        case ReportCommon::TREPORT_FIELDS::timestamp :
        {

            QString str = index.data().toDateTime().toString(("dd-MM-yyyy hh:mm:ss"));
            custom_paint(painter, option, str);

        }
        break;
        case ReportCommon::TREPORT_FIELDS::duration :
        {
            auto hms = pwm_utils::secunds2hms(index.data().toUInt());
            QString str = pwm_utils::hms2string(hms);
            custom_paint(painter, option, str);
        }
        break;
        default:
            QItemDelegate::paint(painter, option, index);
            break;
    }
}


void ReportCommon::report_row_changed(const QModelIndex& current)
{
    QString id_report = ":id_report";
    QVariant rep_id =  m_reports_model->index(current.row(), 0).data();

    if (!m_report_details_model)
    {

        QString query_text  = QString
                              (
                                  " SELECT stage_number,stage_duration, u_beg, i_beg, u_end, i_end, capacity "
                                  " FROM treport_details  where id_report = %1 "
                                  " order by stage_number"
                              ).arg(id_report);
        QSqlQuery query(*rep_database.database());
        m_report_details_model = new ReportDetailsModel(ReportDetailTable);
        query.prepare(query_text);
        query.bindValue(id_report, rep_id);
        query.exec();
        m_report_details_model->setQuery(query);

        m_report_details_model->setHeaderData(0, Qt::Horizontal, QObject::tr("Этап"));
        m_report_details_model->setHeaderData(1, Qt::Horizontal, QObject::tr("Длительность"));
        m_report_details_model->setHeaderData(2, Qt::Horizontal, QObject::tr("U нач"));
        m_report_details_model->setHeaderData(3, Qt::Horizontal, QObject::tr("I нач"));
        m_report_details_model->setHeaderData(4, Qt::Horizontal, QObject::tr("U кон"));
        m_report_details_model->setHeaderData(5, Qt::Horizontal, QObject::tr("I кон"));
        m_report_details_model->setHeaderData(6, Qt::Horizontal, QObject::tr("Емкость"));
        auto hdr = ReportDetailTable->horizontalHeader();
        hdr->setSectionResizeMode(QHeaderView::ResizeToContents);
        hdr->setStretchLastSection(true);
        ReportDetailTable->setModel(m_report_details_model);

    }

    QSqlQuery query =  m_report_details_model->query();
    query.bindValue(id_report, rep_id);
    query.exec();
    m_report_details_model->setQuery(query);
}

void ReportCommon::createRecord()
{

    if (TypesTable->hasFocus())
    {
        qDebug() << Q_FUNC_INFO << " TypesTable focus" ;
    }
    if (NumbersTable->hasFocus())
    {
        qDebug() << Q_FUNC_INFO << " NumbersTable focus" ;
    }
    if (UsersTable->hasFocus())
    {
        qDebug() << Q_FUNC_INFO << " UsersTable focus" ;
    }
}

void ReportCommon::markDelRecord()
{
    if (TypesTable->hasFocus())
    {
        qDebug() << Q_FUNC_INFO << " TypesTable focus" ;
    }
    if (NumbersTable->hasFocus())
    {
        qDebug() << Q_FUNC_INFO << " NumbersTable focus" ;
    }
    if (UsersTable->hasFocus())
    {
        qDebug() << Q_FUNC_INFO << " UsersTable focus" ;
    }
}

void ReportCommon::setAdmin(bool a)
{
    bAdmin = a;
    actUserNew->setEnabled(bAdmin);
    user_row_changed(UsersTable->currentIndex());
}

void ReportCommon::refreshAKB()
{
    setIDAKB(idAKBOld);
}

void ReportCommon::saveAKB()
{
    idAKBOld = getIDAKB();
}

void ReportCommon::setIDAKB(qlonglong idAKB)
{
    if (idAKB > 0)
    {
        QString query_text = QString("SELECT id_type FROM tbattery_list WHERE id = %1 ").arg(idAKB);
        QSqlQuery query(*rep_database.database());
        query.prepare(query_text);
        query.exec();
        query.next();
        QVariant idType = query.value(0);
        QSqlTableModel* tm = rep_database.types_model();
        for (int i = 0; i < tm->rowCount(); i++)
        {
            QSqlRecord rec = tm->record(i);
            if (rec.value("id") == idType)
            {
                TypesTable->setCurrentIndex(TypesTable->model()->index(i, 1));
                rep_database.numbers_select(idType);
                QSqlTableModel* nm = rep_database.numbers_model();
                for (int j = 0; j < nm->rowCount(); j++)
                {
                    QSqlRecord rec = nm->record(j);
                    if (rec.value("id").toLongLong() == idAKB)
                        NumbersTable->setCurrentIndex(NumbersTable->model()->index(j, 2));
                }
                break;
            }
        }
    }
}

qlonglong ReportCommon::getIDAKB()
{
    qlonglong idAKB = 0;
    QModelIndex index = NumbersTable->currentIndex();
    if (index.isValid())
    {
        auto model = index.model();
        idAKB = model->index(index.row(), rep_database.field_id(model)).data().toLongLong();
    }
    return idAKB;
}
