#ifndef MD5_H
#define MD5_H

#include <QString>
#include <QVariant>
#include <QCryptographicHash>

inline QString makeMD5(QString original)
{
    return (QString(QCryptographicHash::hash(QVariant(original).toByteArray(), QCryptographicHash::Md5).toHex()).toLower());
}

#endif // MD5_H
