﻿#include "Login.h"

#include <QMessageBox>
#include <QSqlQuery>

#include "MD5.h"

Login::Login(QWidget* parent) :
	ZrmChannelWidget(parent)
{
    setupUi(this);

    connect(pushButtonLogin, SIGNAL(clicked(bool)), this, SLOT(login()));
}

void Login::clear()
{
    id = 0;
    fio.clear();
    labelUser->setText("Пользователь:");
    bAdmin = false;
    lineEditLogin->clear();
    lineEditPassword->clear();
    lineEditLogin->setFocus();
}

void Login::login()
{
    QString login = lineEditLogin->text();
    QString password = lineEditPassword->text();
    if (login.isEmpty())
    {
        QMessageBox::warning(this, "Внимание", "Пользователь не задан");
        return;
    }
    if (password.isEmpty())
    {
        QMessageBox::warning(this, "Внимание", "Пароль не задан");
        return;
    }

    QString query_text = QString("SELECT * FROM tusers WHERE login = '%1' ;").arg(login);
    QSqlQuery query(*db.database());
    query.prepare(query_text);
    query.exec();
    if (!query.next())
    {
        QMessageBox::warning(this, "Внимание", "Пользователь не найден");
        return;
    }
    QString dbPassword = query.value("password").toString();
    if (makeMD5(password) != dbPassword)
    {
        QMessageBox::warning(this, "Внимание", "Пароль не совпадает");
        return;
    }
    // залогинился
    id = query.value("id").toLongLong();
    fio = query.value("short_fio"). toString();
    labelUser->setText("Пользователь: " + fio);
    bAdmin = query.value("admin").toLongLong();
}
