#ifndef ANDORBUTTON_H
#define ANDORBUTTON_H

#include <QPushButton>

class AndOrButton : public QPushButton
{
    Q_OBJECT

public:

    enum State
    {
        EMPTY,
        OR,
        AND
    };

    AndOrButton(QWidget *parent = nullptr);

    State getState() { return state; }
    void setState(State s);
    bool isChecked() { return state != EMPTY; }

signals:
    void stateChanged(State);

protected slots:
    void onClick();
    void onStateChanged();

private:
    State state = EMPTY;
};

#endif // ANDORBUTTON_H
