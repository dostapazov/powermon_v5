﻿#ifndef LOGIN_H
#define LOGIN_H

#include <zrmbasewidget.h>

#include "ui_Login.h"

#include "zrmreportdatabase.h"

class Login : public ZrmChannelWidget, private Ui::Login
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = nullptr);

    inline bool isAdmin() { return bAdmin; }
    inline QString getFIO() { return fio; }
    inline qlonglong getID() { return id; }

    void clear();

private slots:
    void login();

private:
    ZrmReportDatabase db;
    QString fio;
    bool bAdmin = false;
    qlonglong id = 0;
};

#endif // LOGIN_H
