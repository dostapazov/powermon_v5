﻿#ifndef REPORTCOMMON_H
#define REPORTCOMMON_H

#include "ui_reportcommon.h"
#include <zrmreportdatabase.h>
#include <qtablewidget.h>

class ReportDetailsModel : public QSqlQueryModel
{
public:
    explicit ReportDetailsModel(QObject* parent = nullptr): QSqlQueryModel(parent) {};
    QVariant data(const QModelIndex& index, int role) const override;
};

class ReportCommon : public QWidget, private Ui::ReportCommon
{
    Q_OBJECT

public:
    explicit ReportCommon(QWidget* parent = nullptr);
    ~ReportCommon() override;
    enum TREPORT_FIELDS : int
    {
        id_rep, id_bat, id_user,
        timestamp, user_fio, duration, energy, capacity
    };

    void setAdmin(bool a);
    void refreshAKB();
    void saveAKB();
    void setIDAKB(qlonglong idAKB);
    qlonglong getIDAKB();

public slots:
    void read_reports();

private slots:

    void typesMarkDel();
    void typesCreate();
    void typesApply();
    void typesRevert();

    void numbersMarkDel();
    void numbersCreate();
    void numbersApply();
    void numbersRevert();

    void userMarkDel();
    void userCreate ();
    void userEdit   ();
    void userApply  ();
    void userRevert ();

    void type_row_changed   (const QModelIndex& current);
    void number_row_changed (const QModelIndex& current);
    void report_row_changed (const QModelIndex& current);
    void user_row_changed   (const QModelIndex& current);

    void switch_pages       (int index);
    void showReport();
    void deleteReport();
    void updateReportButtons();

    void createRecord();
    void markDelRecord();

private :
    QVariant recordId(QTableView* table);
    void initReportsModel();
    void init_actions();
    void open_users  ();
    void open_types  ();
    void open_numbers();

    QSqlQuery report_query_get();
    void      report_query_bind_values(QSqlQuery& query);


    QSqlQueryModel*   m_reports_model        = Q_NULLPTR;
    ReportDetailsModel*   m_report_details_model = Q_NULLPTR;
    ZrmReportDatabase rep_database;
    bool bAdmin = false;
    qlonglong idAKBOld = -1;

    void makeShortCuts();
    void initUi();
    void makeSlotConnections();
};

#endif // REPORTCOMMON_H
