#include "localedotreplacer.h"
#include <QKeyEvent>
#include <QLocale>
#include <QDebug>

LocaleDotReplacer::LocaleDotReplacer(QWidget* w, QObject* parent)
    : QObject{parent},
      widget{w}
{
}

bool LocaleDotReplacer::eventFilter(QObject* target, QEvent* event)
{
    if (widget && event->type() == QEvent::KeyPress)
    {
        //Замена точки или запятой в разделителе на соответсвующий в текущей локали
        QKeyEvent* key_event = dynamic_cast<QKeyEvent*>(event);
        if (key_event)
        {
            QLocale locale = widget->locale();
            QChar c(key_event->key());
            if ((c == QChar(',') || c == QChar('.')) &&
                    c != locale.decimalPoint())
            {
                key_event->accept();
                //qDebug() << Q_FUNC_INFO << "decimal point is " << locale.decimalPoint() << "text is " << c;
                c = locale.decimalPoint();
                QKeyEvent newEvent(key_event->type(), c.toLatin1(),
                                   key_event->modifiers(), QString(c));
                target->event(&newEvent);
                return true;
            }
        }
    }
    return QObject::eventFilter(target, event);
}

