﻿#ifndef MOVEMAINWINDOW_H
#define MOVEMAINWINDOW_H

#include <QWidget>


class MoveMainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MoveMainWindow(QWidget* parent = nullptr);

signals:
private:
    void mousePressEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void paintEvent(QPaintEvent* event) override;
    QWidget* mainWindow = nullptr;
    bool pressed = false;
    bool moved   = false;
    QPoint spot;

    void moveMainWindow(const QPoint& newSpot);
};

#endif // MOVEMAINWINDOW_H
