﻿#ifndef USER_EVENTS_HPP
#define USER_EVENTS_HPP
#include <QEvent>

constexpr int POWERMON_EVENT_SHOW_MAXIMIZED = 1 ;
constexpr int POWERMON_EVENT_SHOW_NORMAL = 2 ;

class QUserEvent : public QEvent
{
public:
    explicit QUserEvent(int user_type):
        QEvent(QEvent::Type::User),
        userType{user_type}
    {}
    int getUserType() const {return userType;}
private:
    int userType;
};


#endif // USER_EVENTS_HPP
