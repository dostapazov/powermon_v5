﻿#include "movemainwindow.h"
#include <QApplication>
#include <QMainWindow>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>
#include <QScreen>
#include <QDebug>
#include <user_events.hpp>


MoveMainWindow::MoveMainWindow(QWidget* parent)
    : QWidget{parent}
{

    const QWidgetList widgets = QApplication::topLevelWidgets();
    for (QWidget* item : widgets)
    {
        if (!qobject_cast<QMainWindow*>(item))
            continue;
        mainWindow = item;
        setCursor(Qt::CursorShape::OpenHandCursor);
        break;
    }

}

void MoveMainWindow::mousePressEvent(QMouseEvent* event)
{
    if (mainWindow)
    {
        setCursor(Qt::CursorShape::ClosedHandCursor);
        spot = event->globalPos();
        grabMouse();
        pressed = true;
        moved = false;

    }
    QWidget::mousePressEvent(event);
}

void MoveMainWindow::moveMainWindow(const QPoint& newSpot)
{

    int dx = newSpot.x() - spot.x(), dy = newSpot.y() - spot.y();
    spot = newSpot;
    QRect rect = mainWindow->geometry();
    QPoint center (rect.center().x() + dx, rect.center().y() + dy);
    rect.moveCenter(center);
    mainWindow->setGeometry(rect);
    mainWindow->updateGeometry();

}

void MoveMainWindow::mouseMoveEvent(QMouseEvent* event)
{
    if (pressed)
    {
        if (!moved)
        {
            moved = true;
            qApp->postEvent(mainWindow, new QUserEvent(POWERMON_EVENT_SHOW_NORMAL));
            qApp->processEvents();
        }
        else
        {
            moveMainWindow(event->globalPos());
        }
    }
    QWidget::mouseMoveEvent(event);
}

void MoveMainWindow::mouseReleaseEvent(QMouseEvent* event)
{
    setCursor(Qt::CursorShape::OpenHandCursor);
    if (pressed)
    {
        releaseMouse();
    }
    if (moved)
    {
        qApp->postEvent(mainWindow, new QUserEvent(POWERMON_EVENT_SHOW_MAXIMIZED));
    }
    QWidget::mouseReleaseEvent(event);
}

void MoveMainWindow::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);
    if (mainWindow)
    {
        QPainter painter(this);
        painter.drawText( rect(), mainWindow->windowTitle(), QTextOption(Qt::AlignmentFlag::AlignCenter));
    }
}
