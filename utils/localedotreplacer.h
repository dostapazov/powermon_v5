#ifndef LOCALEDOTREPLACER_H
#define LOCALEDOTREPLACER_H

#include <QObject>
#include <QWidget>

class LocaleDotReplacer : public QObject
{
    Q_OBJECT
public:
    explicit LocaleDotReplacer(QWidget* w, QObject* parent = nullptr);
private:
    bool eventFilter(QObject* target, QEvent* event) override;
    QWidget* widget = nullptr;


};

#endif // LOCALEDOTREPLACER_H
